# ExTools
An ongoing project with the goal of developing an entire modding framework for the game Exanima.
Includes Rayform Parser, and an Unity level editor.
Developed by Corus.

## Installation
* If you are unfamiliar with git, just download clone.bat and run it where you want to clone this repository.
* By launching pull.bat you can update the editor to the last commit.
* Install Unity Hub from the official site
* Install the Unity Editor, I recommend the LTS version labelled 2021.x
* Open Editor in Unity Hub in the newly created folder. It will take some time to initialize the first time.
* On the toolbar top you should see "Exanima", it contains all the custom interfaces I've developed so far.
* Select Install Path to choose your install directory.
* Load a save or a locale, I included a basic save you can use as a template.
Saves are stored in C:\\Users\\[username]\AppData\\Roaming\\Exanima
* When you are done click save on the toolbar to export the locale or save.
* Before launching the game to test it, click on unload, this will unload all files and resources, allowing you to safely run Exanima without quitting Unity.

## TODO
* Right now the editor can load saves and locales and save them back, but I'm planning to add support for exporting custom meshes.
* Most interfaces are clunky and unfinished. I'm doing the best I can for a good user experience.
* Exporting official saves and locales from the editor will probably cause tons of issues. There are still things not being parsed or exported 100% accurately, but I'm planning to make the exporter work flawlessly, no matter what you are working with.

## Credits:
* Jango for helping me understand how tilemaps work.
* Inuk for his documentation that I used as basis for my Rayform Parser.
* Damien for supporting new chunks when Corus can't be bothered
* The Hellmode team for directing me on how certain interfaces and systems should work.
* And thanks to the entire community of Council of Conservers for giving me the much needed motivation to continue this project of pure weaponized autism.
* The voices in my head.