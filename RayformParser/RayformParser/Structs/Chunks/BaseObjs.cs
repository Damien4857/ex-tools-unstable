﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Rayform
{
    public class BaseObjs : Chunk // This is for placing db objects (items, doors, containers) in the locale.
    {
        public List<Node> nodes = new List<Node>();

        public override void ReadStruct(Stream fs)
        {
            //fs.Position = end;
            //return;
            int nodes_n = Parser.ReadInt32(fs);
            for (int i = 0; i < nodes_n; i++)
            {
                Node node = (Node)StartReadChunk(fs);
                node.ReadStruct(fs);
                node.EndReadChunk(fs);
                nodes.Add(node);
            }
        }

        public override void WriteStruct(Stream fs)
        {
            Parser.WriteInt32(fs, nodes.Count);
            foreach (Node node in nodes)
            {
                node.WriteChunk(fs);
            }
        }

        public class Node : Chunk
        {
            public Id id;
            public ObjClass.Flags flags;
            public int unknown2; // No observed effect.
            public Matrix3x4 transform;
            public int unknown3; // In testing, if non-zero, causes position overflow and removes the object
            public int unknown4; // No observed effect. May be related to node_int_c.
                                 // See the "rfcchunk.type == 0x3DE10000" struct for fields similar to the two above.
            public Subnodes subnodes;
            public Subnodes2 subnodes2;

            public override void ReadStruct(Stream fs)
            {
                id = new Id();
                id.ReadStruct(fs);
                flags = (ObjClass.Flags)Parser.ReadUInt32(fs);
                unknown2 = Parser.ReadInt32(fs);
                transform = Parser.ReadMatrix(fs);
                unknown3 = Parser.ReadInt32(fs);
                unknown4 = Parser.ReadInt32(fs);

                Chunk chunk = Chunk.StartReadChunk(fs);
                if (chunk.chunkId == Utils.HexStringtoInt32("AC EC E0 3D"))
                {
                    subnodes2 = (Subnodes2)chunk;
                    subnodes2.ReadStruct(fs);
                    subnodes2.EndReadChunk(fs);
                }
                else if (chunk.chunkId == Utils.HexStringtoInt32("00 EC E0 3D"))
                {
                    subnodes = (Subnodes)chunk;
                    subnodes.ReadStruct(fs);
                    subnodes.EndReadChunk(fs);
                }
            }

            public override void WriteStruct(Stream fs)
            {
                id.WriteStruct(fs);
                Parser.WriteUInt32(fs, (uint)flags);
                Parser.WriteInt32(fs, unknown2);
                Parser.WriteMatrix(fs, transform);
                Parser.WriteInt32(fs, unknown3);
                Parser.WriteInt32(fs, unknown4);

                if (subnodes != null)
                {
                    subnodes.WriteChunk(fs);
                }
                else if (subnodes2 != null)
                {
                    subnodes2.WriteChunk(fs);
                }
            }
        }
    }
}