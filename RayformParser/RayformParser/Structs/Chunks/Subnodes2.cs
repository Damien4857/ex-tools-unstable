using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Rayform
{
    public class Subnodes2 : Chunk
    {
        public int children; // Children directly parented to the target node of this subnode chunk
        public List<Node> nodes = new List<Node>();

        public Str128 name; // The name of a model in objlib.rpk.
        public int unknown1; // In testing, if non-zero, causes position overflow and removes the object
        public Matrix3x4 transform;

        public override void ReadStruct(Stream fs)
        {
            name = Parser.ReadStr128(fs);
            unknown1 = Parser.ReadInt32(fs);
            transform = Parser.ReadMatrix(fs);

            long size = this.chunkSize;
            long progress = 0;
            //children = Parser.ReadInt32(fs);
            //int subnodes_n = Parser.ReadInt32(fs);
            //throw new InvalidOperationException(String.Format("{0}", this.chunkSize));
            for (long i = 0; i < size-68; i+=68)
            {
                Node node = new Node();
                node.ReadStruct(fs);
                nodes.Add(node);
            }
        }

        public override void WriteStruct(Stream fs)
        {
            //Parser.WriteInt32(fs, children);
            //Parser.WriteInt32(fs, nodes.Count);
            Parser.WriteInt32(fs, (int)this.chunkSize);
            foreach (Node node in nodes)
            {
                node.WriteStruct(fs);
            }
        }

        public class Node : Struct
        {
            public int chunkId;
            public int size;
            public Str128 name; // The name of a model in objlib.rpk.
            public int unknown1; // In testing, if non-zero, causes position overflow and removes the object
            public Matrix3x4 transform;
            //public int children; // No observed effect. May be related to unknown1.
            //public int totalChildren; // No observed effect. May be related to unknown1.

            public override void ReadStruct(Stream fs)
            {
                chunkId = Parser.ReadInt32(fs);
                size = Parser.ReadInt32(fs);
                name = Parser.ReadStr128(fs);
                unknown1 = Parser.ReadInt32(fs);
                transform = Parser.ReadMatrix(fs);
                //throw new InvalidOperationException(String.Format("{0} {1} {2}", name, unknown1, transform));
                //children = Parser.ReadInt32(fs); // Children directly parented to this node
                //totalChildren = Parser.ReadInt32(fs); // Recursively all children parented to this node
            }
            public override void WriteStruct(Stream fs)
            {
                Parser.WriteStr128(fs, name);
                Parser.WriteInt32(fs, unknown1);
                Parser.WriteMatrix(fs, transform);
                //Parser.WriteInt32(fs, children);
                //Parser.WriteInt32(fs, totalChildren);
            }
        }
    }
}