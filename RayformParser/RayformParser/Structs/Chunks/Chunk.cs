﻿using Newtonsoft.Json;
using Rayform.Chunks;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Reflection;

namespace Rayform
{
    // Chunk's Funk and Sunk
    public class Chunk : Struct
    {
        [JsonIgnore]
        public long start;
        [JsonIgnore]
        public long chunkSize;
        [JsonIgnore]
        public long end { get { return start + chunkSize; } }

        public static Dictionary<int, Type> chunkDic =
        new Dictionary<int, Type>() {
        { Utils.HexStringtoInt32("00 DB CD CE"), typeof(RdbChars) },
        { Utils.HexStringtoInt32("DB DB 00 00"), typeof(RdbObjs) },
        { Utils.HexStringtoInt32("06 AC CD 00"), typeof(Character) },
        { Utils.HexStringtoInt32("05 AC CD 00"), typeof(Character) },
        { Utils.HexStringtoInt32("00 EC CD 00"), typeof(Character.ArenaLoadouts) },
        { Utils.HexStringtoInt32("00 00 10 A0"), typeof(ArsenalInventory) },
        { Utils.HexStringtoInt32("CD DB 00 00"), typeof(RdbObjStrings) },
        { Utils.HexStringtoInt32("CE DB 00 00"), typeof(BaseObjs) },
        { Utils.HexStringtoInt32("01 DB 00 00"), typeof(BaseObjs.Node) },
        { Utils.HexStringtoInt32("ED 3D 00 00"), typeof(Tilemap) },
        { Utils.HexStringtoInt32("00 CA C0 CA"), typeof(CharacterPosition) },
        { Utils.HexStringtoInt32("00 E1 C0 CA"), typeof(CharacterPosition.BonePositions) },
        { Utils.HexStringtoInt32("00 EA C0 CA"), typeof(CharacterPosition.Unknown) },
        { Utils.HexStringtoInt32("00 01 00 00"), typeof(Exploration) },
        { Utils.HexStringtoInt32("10 00 00 00"), typeof(DiaryEntries) },
        { Utils.HexStringtoInt32("00 00 10 00"), typeof(GlobalXP) },
        { Utils.HexStringtoInt32("00 00 E1 3D"), typeof(LibNodes) },
        { Utils.HexStringtoInt32("00 01 E1 3D"), typeof(LibNodes.Node) },
        { Utils.HexStringtoInt32("00 EC E0 3D"), typeof(Subnodes) },
        { Utils.HexStringtoInt32("AC EC E0 3D"), typeof(Subnodes2) },
        { Utils.HexStringtoInt32("C0 DB 00 00"), typeof(LocaleObjs) },
        { Utils.HexStringtoInt32("01 00 C0 CA"), typeof(LocaleChars) },
        { Utils.HexStringtoInt32("00 DC 00 EC"), typeof(Enviroment) },
        { Utils.HexStringtoInt32("00 BA 00 00"), typeof(Materials) },
        { Utils.HexStringtoInt32("01 BA 00 00"), typeof(Material) },
        { Utils.HexStringtoInt32("00 00 00 3D"), typeof(Scene) },
        { Utils.HexStringtoInt32("03 3D 00 00"), typeof(Mesh) },
        { Utils.HexStringtoInt32("06 3D 00 00"), typeof(LightSource) },
        { Utils.HexStringtoInt32("08 3D 00 00"), typeof(Camera) },
        { Utils.HexStringtoInt32("0A 3D 00 00"), typeof(Sector) },
        { Utils.HexStringtoInt32("0C 3D 00 00"), typeof(VoxelLight) },
        { Utils.HexStringtoInt32("00 3D 00 00"), typeof(Skeleton) },
        { Utils.HexStringtoInt32("01 3D 00 00"), typeof(Node) },
        { Utils.HexStringtoInt32("01 0D AE DB"), typeof(RdbLocales) },
        { Utils.HexStringtoInt32("0F 01 CE AF"), typeof(ItemFactory) },
        { Utils.HexStringtoInt32("01 0D CB DB"), typeof(RdbRoles) },
        { Utils.HexStringtoInt32("C0 3D 00 00"), typeof(Mesh.Prop) },
        { Utils.HexStringtoInt32("C1 3D 00 00"), typeof(Mesh.Prop.Poly) },
        { Utils.HexStringtoInt32("C2 3D 00 00"), typeof(Mesh.Prop.Poly.Faces) },
        { Utils.HexStringtoInt32("C3 3D 00 00"), typeof(Mesh.Prop.Poly.SmoothFaces) },
        { Utils.HexStringtoInt32("05 EC 0C 3D"), typeof(Collision) },
        { Utils.HexStringtoInt32("04 EC 0C 3D"), typeof(Collision2) },
        };
        //[JsonIgnore]
        public int chunkId;

        public static Chunk StartReadChunk(Stream fs)
        {
            Chunk chunk;
            int chunkId;
            uint chunkSize;
            long start = fs.Position;
            chunkId = Parser.ReadInt32(fs);
            chunkSize = Parser.ReadUInt32(fs);

            Type type;
            chunkDic.TryGetValue(chunkId, out type);

            if (type == null)
            {
                Console.WriteLine("ERROR: NO CHUNK FOUND OF ID " + Utils.BytesToHexString(BitConverter.GetBytes(chunkId)));
                chunk = new UnknownChunk();
                chunk.chunkSize = chunkSize;
                chunk.start = fs.Position;
                chunk.chunkId = chunkId;
                return chunk;
            }
            else
            {
                Console.WriteLine("Chunk found - ID: " + Utils.BytesToHexString(BitConverter.GetBytes(chunkId)) + " Size: " + chunkSize + " Type: " + type);
                chunk = Activator.CreateInstance(type, null) as Chunk;
                chunk.chunkSize = chunkSize;
                chunk.start = fs.Position;
                chunk.chunkId = chunkId;
                return chunk;
            }
        }

        public void WriteChunk(Stream fs)
        {
            StartWriteChunk(fs);
            WriteStruct(fs);
            EndWriteChunk(fs);
        }

        public void StartWriteChunk(Stream fs)
        {
            fs.Position += 8;
            start = fs.Position;
        }

        public void EndReadChunk(Stream fs)
        {
            fs.Position = end;
        }

        public void EndWriteChunk(Stream fs)
        {
            chunkSize = fs.Position - start;

            // Go back and write the chunk header
            fs.Position = start - 8;
            int chunkId = chunkDic.FirstOrDefault(x => x.Value == GetType()).Key;
            if (chunkId != 0)
            {
                this.chunkId = chunkId;
            }
            Parser.WriteInt32(fs, this.chunkId);
            Parser.WriteUInt32(fs, (uint)chunkSize);

            // Return
            fs.Position = start + chunkSize;
        }

        public override void ReadStruct(Stream fs)
        {
            Console.WriteLine("read not supported yet " + GetType());
            fs.Position += chunkSize;
        }
        public override void WriteStruct(Stream fs)
        {
            Console.WriteLine("write not supported yet " + GetType());
        }
    }
}
