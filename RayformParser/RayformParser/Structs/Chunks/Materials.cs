﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Rayform
{
    public class Materials : Chunk // These materials are then added to some kind of material db
    {
        public List<Material> materials = new List<Material>();
        public override void ReadStruct(Stream fs)
        {
            int material_n = Parser.ReadInt32(fs);
            for (int i = 0; i < material_n; i++)
            {
                // I need to figure this shit out
                //Material material = (Material)Chunk.StartReadChunk(fs);
                //material.ReadStruct(fs);
                //materials.Add(material);

                Chunk chunk = Chunk.StartReadChunk(fs);
                chunk.ReadStruct(fs);
                chunk.EndReadChunk(fs);
                if (chunk.GetType() == typeof(Material))
                {
                    materials.Add((Material)chunk);
                }
            }
        }

		public override void WriteStruct(Stream fs)
		{
            Parser.WriteInt32(fs, materials.Count);
            foreach (Material material in materials)
            {
                material.WriteChunk(fs);
            }
		}
	}
}
