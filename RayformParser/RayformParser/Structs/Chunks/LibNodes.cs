﻿using System;
using System.Collections.Generic;
using System.IO;
using static Rayform.CharacterPosition;

namespace Rayform
{
    public class LibNodes : Chunk
    {
        public List<Node> nodes = new List<Node>();

        public override void ReadStruct(Stream fs)
        {
            int nodes_n = Parser.ReadInt32(fs);
            for(int i = 0; i < nodes_n ; i++)
            {
                Node node = (Node)Chunk.StartReadChunk(fs);
                node.ReadStruct(fs);
                node.EndReadChunk(fs);
                nodes.Add(node);
            }
        }

        public override void WriteStruct(Stream fs)
        {
            Parser.WriteInt32(fs, nodes.Count);
            foreach (Node node in nodes)
            {
                node.WriteChunk(fs);
            }
        }

        public class Node : Chunk
        {
            public Flags flags; // Flag field, typically null. Flags present in the flags field: 0x80000 - The libnode is immovable. Other flags are not observed to have an effect.
            public enum Flags : uint
            {
                Movable = 0,
                Immovable = 0x80000
            }
            public Str128 name; // The name of a model in objlib.rpk.
            public Matrix3x4 transform;
            public int unknown1; // In testing, if non-zero, causes position overflow and removes the object
            public int unknown2; // No observed effect. May be related to unknown1.

            public Subnodes subnodes;
            public Subnodes2 subnodes2;
            //public List<Subnodes2> subnodes2 = new List<Subnodes2>();

            public override void ReadStruct(Stream fs)
            {
                flags = (Flags)Parser.ReadUInt32(fs);
                name = Parser.ReadStr128(fs);
                transform = Parser.ReadMatrix(fs);
                unknown1 = Parser.ReadInt32(fs);
                unknown2 = Parser.ReadInt32(fs);

                Chunk chunk = Chunk.StartReadChunk(fs);
                if (chunk.chunkId == Utils.HexStringtoInt32("AC EC E0 3D"))
                {
                    long position = fs.Position;
                    //bool first = true;
                    //while (fs.Position < position + this.chunkSize)
                    //for (long i = position; i < position + chunkSize; i+=fs.Position - position)
                    {
                        //if (!first)chunk=Chunk.StartReadChunk(fs);
                        //first = false;
                        //Subnodes2 node = (Subnodes2)chunk;
                        subnodes2 = (Subnodes2)chunk;
                        //node.ReadStruct(fs);
                        subnodes2.ReadStruct(fs);
                        //node.EndReadChunk(fs);
                        subnodes2.EndReadChunk(fs);
                        //subnodes2.Add(node);
                    }
                }
                else if (chunk.chunkId == Utils.HexStringtoInt32("00 01 E1 3D"))
                {
                    Node node = (Node)chunk;
                    node.ReadStruct(fs);
                    node.EndReadChunk(fs);
                }
                else if (chunk.chunkId == Utils.HexStringtoInt32("00 EC E0 3D"))
                {
                    subnodes = (Subnodes)chunk;
                    subnodes.ReadStruct(fs);
                    subnodes.EndReadChunk(fs);
                }
            }

            public override void WriteStruct(Stream fs)
            {
                Parser.WriteUInt32(fs, (uint)flags);
                Parser.WriteStr128(fs, name);
                Parser.WriteMatrix(fs, transform);
                Parser.WriteInt32(fs, unknown1); //
                Parser.WriteInt32(fs, unknown2); //^
                if (subnodes != null)
                {
                    subnodes.WriteChunk(fs);
                }
                else if (subnodes2 != null)
                {
                    //foreach(Subnodes2 subnodes2 in subnodes2)
                    subnodes2.WriteChunk(fs);
                }
            }
        }
    }
}
