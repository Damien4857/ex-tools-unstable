﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rayform
{
    public class DiaryEntries : Chunk
    {
        public List<Entry> entries = new List<Entry>();

        public override void ReadStruct(Stream fs)
        {
            int entries_n = Parser.ReadInt32(fs);
            for (int i = 0; i < entries_n; i++)
            {
                Entry entry = new Entry();
                entry.ReadStruct(fs);
                entries.Add(entry);
            }
        }

        public override void WriteStruct(Stream fs)
        {
            Parser.WriteInt32(fs, entries.Count);
            foreach (Entry entry in entries)
            {
                entry.WriteStruct(fs);
            }
        }

        public class Entry : Chunk
        {
            public int unknown;
            public string title;
            public string content;

            public override void ReadStruct(Stream fs)
            {
                unknown = Parser.ReadInt32(fs);
                int titleLength = Parser.ReadInt32(fs);
                title = Parser.ReadString(fs, titleLength);
                int contentLength = Parser.ReadInt32(fs);
                content = Parser.ReadString(fs, contentLength);
            }

            public override void WriteStruct(Stream fs)
            {
                Parser.WriteInt32(fs, unknown);
                Parser.WriteInt32(fs, title.Length);
                Parser.WriteString(fs, title);
                Parser.WriteInt32(fs, content.Length);
                Parser.WriteString(fs, content);
            }
        }
    }
}
