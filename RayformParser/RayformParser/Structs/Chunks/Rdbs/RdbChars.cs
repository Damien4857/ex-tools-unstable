﻿using Newtonsoft.Json;
using Rayform;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;

namespace Rayform
{
    public class RdbChars : Rdb
    {
        public override object ReadRdbData(Stream fs, uint size)
        {
            Chunk chunk = Chunk.StartReadChunk(fs);
            chunk.ReadStruct(fs);
            chunk.EndReadChunk(fs);
            return chunk;
        }

        public override void WriteRdbData(Stream fs, object data)
        {
            ((Chunk)data).WriteChunk(fs);
        }

        // The struct used for rdb indexing
        //public new class Entry : Rdb.Entry
        //{
        //    public new Character data;
        //}
    }
}