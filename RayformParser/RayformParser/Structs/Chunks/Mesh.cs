﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;

namespace Rayform
{
    public class Mesh : Scene.Node
    {
        public int GeomFlag;
        public Vector3[] vrtDats;
        public Vector2[] uvDats;
        public BoneWeight[] weightDats;
        public int[] unkDats;
        public List<Chunk> chunks = new List<Chunk>();
        public byte[] RawNodeData;
        public override void ReadNode(Stream fs)
        {
            //start = fs.Position;
            //objname = Parser.ReadStr128(fs);
            //nodeflags = Parser.ReadInt32(fs);
            //objFlags = Parser.ReadInt32(fs);
            //suppress = Parser.ReadInt32(fs);
            //parent = Parser.ReadInt32(fs);
            //obj_transform = Parser.ReadMatrix(fs);
            //obj_vec_a = Parser.ReadVector3(fs);
            //obj_vec_b = Parser.ReadVector3(fs);
            //obj_float_a = Parser.ReadFloat(fs);
            //obj_int_b = Parser.ReadInt32(fs);
            //sometimes the mesh has no data. why?
            if(fs.Position>=end)
            {
                return;
            }
            long nodeDataStart = fs.Position; //This makes me want to die

            GeomFlag = Parser.ReadInt32(fs);

            int dats_n = Parser.ReadInt32(fs) + 1;
            for (int i = 0; i < dats_n; i++)
            {
                int vrtFlags = Parser.ReadInt32(fs);
                int type = Parser.ReadInt32(fs);
                int count = Parser.ReadInt32(fs) + 1;
                if (type == BitConverter.ToInt32(Utils.HexStringtoBytes("01 00 F3 00")))
                {
                    vrtDats = new Vector3[count];
                    for (int j = 0; j < count; j++)
                    {
                        vrtDats[j] = Parser.ReadVector3(fs);
                    }

                }
                else if (type == BitConverter.ToInt32(Utils.HexStringtoBytes("10 00 F2 00")))
                {
                    uvDats = new Vector2[count];
                    for (int j = 0; j < count; j++)
                    {
                        uvDats[j] = Parser.ReadVector2(fs);
                    }
                }
                else if (type == BitConverter.ToInt32(Utils.HexStringtoBytes("00 01 B4 02")))
                {
                    unkDats = new int[count];
                    for (int j = 0; j < count; j++)
                    {
                        unkDats[j] = Parser.ReadInt32(fs);
                    }
                }
                else if (type == BitConverter.ToInt32(Utils.HexStringtoBytes("00 10 A4 02")))
                {
                    weightDats = new BoneWeight[count];
                    for (int j = 0; j < count; j++)
                    {
                        BoneWeight weight = new BoneWeight();
                        weight.ReadStruct(fs);
                        weightDats[j] = weight;
                    }
                }
            }
            //System.Diagnostics.Debug.WriteLine(fs.Position);
            while (fs.Position < end)
            {
                Chunk chunk = Chunk.StartReadChunk(fs);
                chunk.ReadStruct(fs);
                chunk.EndReadChunk(fs);
                chunks.Add(chunk);
            }
            int nodeDataLength = (int)(end - nodeDataStart);
            RawNodeData = new byte[nodeDataLength];
            fs.Position = nodeDataStart;
            fs.Read(RawNodeData, 0, nodeDataLength);

            fs.Position = end;
        }
//disabled parser writing, replaced with method to save all raw mesh data
         public override void WriteNode(Stream fs)
         {
            fs.Write(RawNodeData, 0, RawNodeData.Length);
            //            // Write mesh-specific data
            //            Parser.WriteInt32(fs, GeomFlag);
            //
            //            int dataCount = 0; // Initialize the count of data sections
            //            if (vrtDats != null)
            //                dataCount++;
            //            if (uvDats != null)
            //                dataCount++;
            //            if (unkDats != null)
            //                dataCount++;
            //            if (weightDats != null)
            //                dataCount++;
            //
            //            Parser.WriteInt32(fs, dataCount - 1);
            //
            //            if (vrtDats != null)
            //            {
            //                Parser.WriteInt32(fs, vrtDats.Length - 1);
            //                for (int i = 0; i < vrtDats.Length; i++)
            //                {
            //                    Parser.WriteVector3(fs, vrtDats[i]);
            //                }
            //            }
            //
            //            if (uvDats != null)
            //            {
            //                Parser.WriteInt32(fs, uvDats.Length - 1);
            //                for (int i = 0; i < uvDats.Length; i++)
            //                {
            //                    Parser.WriteVector2(fs, uvDats[i]);
            //                }
            //            }
            //
            //            if (unkDats != null)
            //            {
            //                Parser.WriteInt32(fs, unkDats.Length - 1);
            //                for (int i = 0; i < unkDats.Length; i++)
            //                {
            //                    Parser.WriteInt32(fs, unkDats[i]);
            //                }
            //            }
            //
            //            if (weightDats != null)
            //            {
            //                Parser.WriteInt32(fs, weightDats.Length - 1);
            //                for (int i = 0; i < weightDats.Length; i++)
            //                {
            //                    weightDats[i].WriteStruct(fs);
            //                }
            //            }
            //
            //            foreach (Chunk chunk in chunks)
            //            {
            //                chunk.WriteStruct(fs);
            //                chunk.EndWriteChunk(fs);
            //            }
            //        }

            //public class BoneWeight
            //{
            //    public int boneId;
            //    public float influence;
        }

        public class Prop : Chunk
        {
            public int unknown;
            public int cmpVrts;
            public int maps_n; // Store maps_n as a class member
            public List<VrtGroup> vrtGroups = new List<VrtGroup>();
            public int[] vrtMaps;
            public int[] uvMaps;
            public int[] unkMaps;
            public Poly poly;
            private int vmFlags; // Store the vmFlags

            public override void ReadStruct(Stream fs)
            {
                start = fs.Position;
                unknown = Parser.ReadInt32(fs);
                cmpVrts = Parser.ReadInt32(fs) + 1;

                int vrtGroups_n = Parser.ReadInt32(fs) + 1;
                for (int i = 0; i < vrtGroups_n; i++)
                {
                    VrtGroup vrtGroup = new VrtGroup();
                    vrtGroup.ReadStruct(fs);
                    vrtGroups.Add(vrtGroup);
                }

                maps_n = Parser.ReadInt32(fs) + 1; // Store maps_n
                for (int i = 0; i < maps_n; i++)
                {
                    System.Diagnostics.Debug.WriteLine(fs.Position);
                    vmFlags = Parser.ReadInt32(fs); // Store the vmFlags
                    if (vmFlags == BitConverter.ToInt32(Utils.HexStringtoBytes("01 00 01 00")) || vmFlags == BitConverter.ToInt32(Utils.HexStringtoBytes("01 00 00 00")))
                    {
                        vrtMaps = new int[cmpVrts];
                        for (int j = 0; j < cmpVrts; j++)
                        {
                            if (cmpVrts < 0xFFFF)
                            {
                                vrtMaps[j] = Parser.ReadInt16(fs);
                            }
                            else
                            {
                                vrtMaps[j] = Parser.ReadInt32(fs);
                            }
                        }
                    }
                    else if (vmFlags == BitConverter.ToInt32(Utils.HexStringtoBytes("10 00 01 00")) || vmFlags == BitConverter.ToInt32(Utils.HexStringtoBytes("10 00 00 00")))
                    {
                        uvMaps = new int[cmpVrts];
                        for (int j = 0; j < cmpVrts; j++)
                        {
                            if (cmpVrts < 0xFFFF)
                            {
                                uvMaps[j] = Parser.ReadInt16(fs);
                            }
                            else
                            {
                                uvMaps[j] = Parser.ReadInt32(fs);
                            }
                        }
                    }
                    else if (vmFlags == BitConverter.ToInt32(Utils.HexStringtoBytes("02 00 00 00")))
                    {
                        unkMaps = new int[cmpVrts];
                        for (int j = 0; j < cmpVrts; j++)
                        {
                            if (cmpVrts < 0xFFFF)
                            {
                                unkMaps[j] = Parser.ReadInt16(fs);
                            }
                            else
                            {
                                unkMaps[j] = Parser.ReadInt32(fs);
                            }
                        }
                    }
                }

                poly = (Poly)Chunk.StartReadChunk(fs);
                poly.ReadStruct(fs, cmpVrts, vrtGroups_n);
                poly.EndReadChunk(fs);
            }
            public override void WriteStruct(Stream fs)
            {

            }
//disabled parser writing
//            public override void WriteStruct(Stream fs)
//            {
//                fs.Position += 8;
//                start = fs.Position;
//
//                Parser.WriteInt32(fs, unknown);
//                Parser.WriteInt32(fs, cmpVrts - 1);
//
//                Parser.WriteInt32(fs, vrtGroups.Count - 1);
//                foreach (VrtGroup vrtGroup in vrtGroups)
//                {
//                    vrtGroup.WriteStruct(fs);
//                }
//
//                Parser.WriteInt32(fs, maps_n);
//                if (vrtMaps != null)
//                {
//                    int vmFlags = (vrtMaps.Length < 0xFFFF) ? 0x00010001 : 0x10000000;
//                    Parser.WriteInt32(fs, vmFlags);
//                    //Parser.WriteInt32(fs, vrtMaps.Length - 1);
//                    for (int i = 0; i < vrtMaps.Length; i++)
//                    {
//                        if (vrtMaps.Length < 0xFFFF)
//                        {
//                            Parser.WriteInt16(fs, (short)vrtMaps[i]);
//                        }
//                        else
//                        {
//                            Parser.WriteInt32(fs, vrtMaps[i]);
//                        }
//                    }
//                }
//
//                if (uvMaps != null)
//                {
//                    int vmFlags = (uvMaps.Length < 0xFFFF) ? 0x00010001 : 0x10000000;
//                    Parser.WriteInt32(fs, vmFlags);
//                    //Parser.WriteInt32(fs, uvMaps.Length - 1);
//                    for (int i = 0; i < uvMaps.Length; i++)
//                    {
//                        if (uvMaps.Length < 0xFFFF)
//                        {
//                            Parser.WriteInt16(fs, (short)uvMaps[i]);
//                        }
//                        else
//                        {
//                            Parser.WriteInt32(fs, uvMaps[i]);
//                        }
//                    }
//                }
//
//                if (unkMaps != null)
//                {
//                    int vmFlags = (unkMaps.Length < 0xFFFF) ? 0x00020000 : 0x20000000;
//                    Parser.WriteInt32(fs, vmFlags);
//                    //Parser.WriteInt32(fs, unkMaps.Length - 1);
//                    for (int i = 0; i < unkMaps.Length; i++)
//                    {
//                        if (unkMaps.Length < 0xFFFF)
//                        {
//                            Parser.WriteInt16(fs, (short)unkMaps[i]);
//                        }
//                        else
//                        {
//                            Parser.WriteInt32(fs, unkMaps[i]);
//                        }
//                    }
//                }
//                //Parser.WriteInt32(fs, maps_n - 1);
//                poly.WriteStruct(fs);
//
//                chunkSize = fs.Position - start;
//
//                // Go back and write the chunk header
//                fs.Position = start - 8;
//                fs.Write(BitConverter.GetBytes(0x003D3DC0), 0, sizeof(int));
//                Parser.WriteUInt32(fs, (uint)chunkSize);
//
//                fs.Position = start + chunkSize;
//            }
//
//
//
            public class Poly : Chunk
            {
                public int ignored;
                public int[] triangles;
                public List<Submesh> submeshes = new List<Submesh>();
                public List<Chunk> chunks = new List<Chunk>();
                //public uint cmpVrts { private get; set; }

                public void ReadStruct(Stream fs, int cmpVrts, int vrtGroups_n)
                {
                    ignored = Parser.ReadInt32(fs);
                    int faces_n = Parser.ReadInt32(fs)+1;


                    // Indices into the vertex VrtMaps array, which is in turn indices into vertices, i.e.v_data_vert[vm_data_vert[face_indices[n]]].
                    // Also used for UVs, since they're not per vertex, but rather per triangle loop,
                    // which means for each triangle t and each vertex v therein, use as v_data_uv[vm_data_uv[triangle[t][v]]].
                    triangles = new int[faces_n*3];
                    for (int i = 0; i < faces_n*3; i++)
                    {
                        if (cmpVrts < 0xFFFF) // Technique to halve the size of this array if there's a relatively small amount of items.
                        {
                            triangles[i] = Parser.ReadUInt16(fs);
                        }
                        else
                        {
                            triangles[i] = Parser.ReadInt32(fs);
                        }
                    }

                    for (int i = 0; i < vrtGroups_n; i++)
                    {
                        Submesh submesh = new Submesh();
                        submesh.ReadStruct(fs);
                        submeshes.Add(submesh);
                    }

                    while (fs.Position < end)
                    {
                        Chunk chunk = Chunk.StartReadChunk(fs);
                        chunk.ReadStruct(fs);
                        chunk.EndReadChunk(fs);
                        chunks.Add(chunk);
                    }
                }
//disabled parser writing
//                public override void WriteStruct(Stream fs)
//                {
//                    // Write the ignored field
//                    Parser.WriteInt32(fs, ignored);
//
//                    // Write the number of triangles
//                    int faces_n = triangles.Length / 3;
//                    Parser.WriteInt32(fs, faces_n - 1);
//
//                    // Write the triangles data
//                    for (int i = 0; i < triangles.Length; i++)
//                    {
//                        if (triangles.Length < 0xFFFF)
//                        {
//                            Parser.WriteUInt16(fs, (ushort)triangles[i]);
//                        }
//                        else
//                        {
//                            Parser.WriteInt32(fs, triangles[i]);
//                        }
//                    }
//
//                    // Write submeshes
//                    foreach (Submesh submesh in submeshes)
//                    {
//                        submesh.WriteStruct(fs);
//                    }
//                }

                public class Faces : Chunk
                {
                    public List<byte> flags = new List<byte>();
                    public override void ReadStruct(Stream fs)
                    {
                        while (fs.Position < end)
                        {
                            flags.Add(Parser.ReadInt8(fs));
                        }
                    }
                }
                public class SmoothFaces : Chunk
                {
                    public List<uint> smoothGrp = new List<uint>();
                    public override void ReadStruct(Stream fs)
                    {
                        while (fs.Position < end)
                        {
                            smoothGrp.Add(Parser.ReadUInt32(fs));
                        }
                    }
//disabled parser writing
//					public override void WriteStruct(Stream fs)
//					{
//						foreach(uint thing in smoothGrp)
//                        {
//                            Parser.WriteUInt32(fs, thing);
//                        }
//                    }
				}
            }
        }
        public class VrtGroup : Struct
        {
            // This defines vertex groups of materials using ranges of[start, stop] (both inclusive boundaries.)
            // Vertices are accessed, like for faces, using the vertex VrtMap.
            public int ignored;
            public int start;
            public int end;
            public Str128 name_material; // Function called: TMaterialManager3.GetMaterial

			public override void ReadStruct(Stream fs)
            {
				ignored = Parser.ReadInt32(fs);
				start = Parser.ReadInt32(fs);
				end = Parser.ReadInt32(fs);
				name_material = Parser.ReadStr128(fs);
			}
//disabled parser writing
//			public override void WriteStruct(Stream fs)
//			{
//				Parser.WriteInt32(fs, ignored);
//				Parser.WriteInt32(fs, start);
//				Parser.WriteInt32(fs, end);
//				Parser.WriteStr128(fs, name_material);
//			}
		}

        public class Submesh : Struct
        {
            public int start;
            public int end;

			public override void ReadStruct(Stream fs)
			{
				start = Parser.ReadInt32(fs);
                end = Parser.ReadInt32(fs);
			}
//disabled parser writing

//			public override void WriteStruct(Stream fs)
//			{
//				Parser.WriteInt32(fs, start);
//                Parser.WriteInt32(fs, end);
//			}
		}

        public class BoneWeight : Struct
        {
            public int[] boneId = new int[4];
            public float[] influence = new float[4];

            public override void ReadStruct(Stream fs)
            {
                for(int i = 0; i < boneId.Length; i++)
                {
                    boneId[i] = Parser.ReadInt32(fs);
                }
                for (int i = 0; i < influence.Length; i++)
                {
                    influence[i] = Parser.ReadFloat(fs);
                }
            }

            public override void WriteStruct(Stream fs)
            {

            }
        }
    }
}