﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Rayform
{
    public class ObjClass : Struct
    {
        public List<HeavyChunk> classes = new List<HeavyChunk>();
        public void ReadStruct(Stream fs, uint size)
        {
            long start = fs.Position;
            while (fs.Position < start + size)
            {
                HeavyChunk heavyChunk = HeavyChunk.StartReadHeavyChunk(fs);
                heavyChunk.ReadStruct(fs);
                heavyChunk.EndReadHeavyChunk(fs);
                classes.Add(heavyChunk);
            }
        }
        public override void WriteStruct(Stream fs)
        {
            foreach (HeavyChunk heavyChunk in classes)
            {
                heavyChunk.WriteHeavyChunk(fs);
            }
        }

        public enum Flags : uint
        {
            None = 0,
            // Marks a door object as locked so it can't be dragged.
            // Doors can still be torn open with enough force, as from a large creature slamming into it.
            // This can be applied on already open doors, to make them undraggable but still movable.
            Locked = 0x1,
            Undraggable = 0x10, // The object is immovable when placed into the world and can not be dragged.
            Held = 0x100, // The object is held in hand. The item is invisible, but still present, in the inventory.
                          // It's set when you drag a weapon into a character's hands, and allows the item to be restored in the same spot when that held item should be put back in the inventory, either by dragging or switching loadout.
                          // As a failsafe, if an item is flagged but somehow isn't in hand, it's unset on save load so the item becomes visible again.
                          // This occurs to dice items with corrupted flags on a table in the level 3 brewery, where if you take them, they vanish into the inventory, but reappear on save load.
            InHub = 0x200, // Arena arsenal item in hub.The item is greyed out, meaning it's put on display in the hub.
            OnRoster = 0x400, // Arena arsenal item on roster.The item has an orange highlight, meaning it's currently used on a character.
            IconScale = 0x4000_0000, //Unknown flag, will double the size of a potion's inventory icon after drinking.
            Used = 0xC80000 // Item will switch name and descriptions to the "WEAR00" variants, will change appearance based on "WEAR00" data in .rfc, and disables interaction besides inspection.
        }
    }
}
