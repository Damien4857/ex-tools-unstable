﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Rayform
{
    public class Utils
    {
        public static string BytesToHexString(byte[] bytes)
        {
            return BitConverter.ToString(bytes).Replace('-', ' ');
        }

        public static byte[] HexStringtoBytes(string str)
        {
            String[] arr = str.Split(' ');
            byte[] array = new byte[arr.Length];
            for (int i = 0; i < arr.Length; i++) array[i] = Convert.ToByte(arr[i], 16);

            return array;
        }

        public static int HexStringtoInt32(string str)
        {
            Byte[] bytes = HexStringtoBytes(str);
            return BitConverter.ToInt32(bytes, 0);
        }

        public static short HexStringtoInt16(string str)
        {
            Byte[] bytes = HexStringtoBytes(str);
            return BitConverter.ToInt16(bytes, 0);
        }

        public static byte[] GetBytes(object aux)
        {
            int length = Marshal.SizeOf(aux);
            IntPtr ptr = Marshal.AllocHGlobal(length);
            byte[] myBuffer = new byte[length];

            Marshal.StructureToPtr(aux, ptr, true);
            Marshal.Copy(ptr, myBuffer, 0, length);
            Marshal.FreeHGlobal(ptr);

            return myBuffer;
        }

        public static FieldInfo[] GetFields(Object obj)
        {
            FieldInfo[] fields = obj.GetType()
            .GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.DeclaredOnly)
            .Where(f => f.GetCustomAttribute<CompilerGeneratedAttribute>() == null)
            .ToArray();

            return fields;
        }
    }
}