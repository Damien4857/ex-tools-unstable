﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Rayform
{
    public class Operative : HeavyChunk // 00 00 01 00
    {
        // TODO: Add categories for triggerType to unlock certain operativeType menus
        public TriggerType triggerType;
        public MechanismType mechanismType;
        public MechanismFlag mechanismFlag;
        public OperativeType operativeType;
        public byte mechanismUnk1;
        public byte mechanismUnk2;
        public byte mechanismUnk3;
        public byte mechanismUnk4;
        public byte mechanismUnk5;
        public int mechanismUnk6;

        public byte eventFlag;
        public byte eventUnk1;
        public byte eventUnk2;
        public byte eventUnk3;
        public byte eventUnk4;
        public byte eventUnk5;
        public byte narratorUnk1;
        public byte narratorUnk2;
        public byte narratorUnk3;
        public byte narratorUnk4;

        public byte brazierFlag;
        public byte brazierUnk1;
        public int brazierUnk2;
        public byte brazierUnk3;
        public byte brazierUnk4;
        public byte brazierUnk5;
        public byte brazierUnk6;
        public int brazierUnk7;

        public byte keyFlag;
        public byte keyUnk1;
        public byte keyUnk2;
        public byte keyUnk3;
        public byte keyUnk4;
        public byte keyUnk5;

        public byte salveFlag;
        public byte salveUnk1;
        public byte salveHealingTime;
        public byte salveUnk3;
        public byte salveUnk4;
        public byte salveUnk5;

        public byte compassFlag;
        public byte compassUnk1;
        public byte compassUnk2;
        public byte compassUnk3;
        public byte compassUnk4;
        public byte compassUnk5;

        public byte clickTransitionFlag;
        public byte clickTransitionUnk1;
        public byte clickTransitionUnk2;
        public byte clickTransitionUnk3;
        public byte clickTransitionUnk4;
        public byte clickTransitionUnk5;

        public LeverHeldFlag leverHeldFlag;
        public byte leverHeldUnk1;
        public byte leverHeldUnk2;
        public byte leverHeldUnk3;
        public byte leverHeldTime;
        public byte leverHeldUnk4;
        public int leverHeldUnk5;

        public PressurePlateFlag pressurePlateFlag;
        public byte plateUnk1;
        public ushort plateUnk2;
        public byte plateUnk4;
        public byte plateUnk5;

        public byte fireFlag;
        public byte fireUnk1;
        public ushort fireSize;
        public byte fireUnk2;
        public byte fireUnk3;

        public byte lightningFlag;
        public LightningAnimationType lightningAnimationType;
        public bool lightningOn;
        public byte lightningUnk1;
        public byte lightningUnk2;
        public byte lightningUnk3;
        public byte lightningUnk4;
        public byte lightningWidth;
        public ushort lightningLength;
        public ushort lightningIntensity;
        public ushort lightningUnk5;

        public byte divinersDeckFlag;
        public byte divinersDeckUnk1;
        public byte divinersDeckUnk2;
        public byte divinersDeckUnk3;
        public byte divinersDeckUnk4;
        public byte divinersDeckUnk5;
        public byte divinersDeckUnk6;
        public byte divinersDeckUnk7;
        public ushort divinersDeckUnk8;
        public int divinersDeckUnk9;


        public byte elixirFlag;
        public byte elixirUnk1;
        public byte elixirUnk2;
        public byte elixirUnk3;
        public byte elixirUnk4;
        public byte elixirUnk5;
        public int elixirUnk6;
        public int elixirUnk7;

        public byte brushScrubFlag;
        public byte brushScrubUnk1;
        public byte brushScrubUnk2;
        public byte brushScrubUnk3;
        public byte brushScrubUnk4;
        public byte brushScrubUnk5;
        public int brushScrubUnk6;
        public int brushScrubUnk7;

        public byte repairSphereFlag;
        public byte repairSphereUnk1;
        public byte repairSphereUnk2;
        public byte repairSphereUnk3;
        public byte repairSphereUnk4;
        public byte repairSphereUnk5;
        public int repairSphereUnk6;
        public int repairSphereUnk7;

        public Id targetId1 = new Id();
        public Id targetId2 = new Id();
        public Str128 sound;

        public enum TriggerType : byte
        {
            Misc = 0, // Being 0, it might just mean no special functionality is attached
            Item = 1,
            Special = 2,
            Lever = 3,
            Button = 4,
            Effect = 5,
            Mechanism = 6,
            Brazier = 7,
            Event = 8,
        }
        public enum MechanismType : byte
        {
            OneTarget = 0, // if MechanismType == Default (0), read controls & targetId1.
            Unknown1 = 1,
            TwoTargets = 2,
            Unknown3 = 3,
            Unknown4 = 4,
            SuccessOrFailCondition = 5, // Observed in P8 Mechanism in level 1. If SuccessOrFailCondition, read controls & targetId1 & targetId2


        }
        public enum MechanismFlag : byte
        {
            None = 0,
            Unknown1 = 1,
            Unknown2 = 2,
            Unknown3 = 3,
            Unknown4 = 4,
            Unknown5 = 5,
            Unknown6 = 6,
            Unknown7 = 7,
            TwoLeversDown = 8,
        }
        public enum OperativeType : byte
        {
            Key = 0,
            Salve = 1,
            Compass = 2,
            ClickableTransition = 3,
            LeverHeld = 4,
            Fire = 5,
            LeverSpecial = 6,
            Lightning = 7,
            SewerLamp = 8,
            DivinersDeck = 9,
            Elixir = 10,
            BrushScrub = 11,
            RepairSphere = 12,
            Unknown13 = 13,
            Narrator = 14,
        }
        public enum OprtvCategoryMisc : byte
        {
            ClickableTransition = 3,
            SecretSwitch = 4,
            DivinersDeck = 9,
        }
        public enum OprtvCategorySpecial : byte
        {
            Crystal = 0,
            Compass = 2,
        }
        public enum OprtvCategoryItem : byte
        {
            Key = 0,
            Salve = 1,
            SpecialKey = 4,
            Elixir = 10,
            BrushScrub = 11,
            RepairSphere = 12,
            Unknown13 = 13,
        }
        public enum OprtvCategoryLever : byte
        {
            Simple = 4,
            TimedFire = 5,
            Special = 6,
            SewerLamp = 8,
        }
        public enum OprtvCategoryBrazier : byte
        {
            TriggerFire = 5,
        }
        public enum OprtvCategoryEvent : byte
        {
            UnlockProctor = 0,
            Narrator = 14,
        }
        public enum OprtvCategoryPressurePlate : byte
        {
            None = 0,
            Button = 4,
            ButtonSpecial = 6,
        }
        public enum PressurePlateFlag : byte
        {
            PushValue = 0,
            Activate = 2,
            Unknown1 = 6,
            Weighted = 22 // Only pressure plate in the game is observed to trigger two objects.
        }
        public enum OprtvCategoryEffect : byte
        {
            Fire = 5,
            Lightning = 7,
        }
        public enum LeverHeldFlag : byte //could be a bitmask
        {
            None = 0,
            RestingDown = 1, // below only for levers
            RestingUp = 2,
            CandleLeverFlag = 3,
            CrossroadsSecretRoomFlag = 10,
            TwoTargets = 16, // nibble or bitmask?
            TwoTargetsInverse = 17,

        }
        public enum LightningAnimationType
        {
            Straight = 2,
            Waving = 4,
        }
        public override void ReadStruct(Stream fs)
        {
            triggerType = (TriggerType)Parser.ReadInt8(fs);
            if (triggerType == TriggerType.Mechanism) // If TriggerType == Mechanism, then read control mechanism controls. Else 
            {
                mechanismType = (MechanismType)Parser.ReadInt8(fs);
                mechanismFlag = (MechanismFlag)Parser.ReadInt8(fs);
                mechanismUnk1 = Parser.ReadInt8(fs);
                mechanismUnk2 = Parser.ReadInt8(fs);
                mechanismUnk3 = Parser.ReadInt8(fs);
                mechanismUnk4 = Parser.ReadInt8(fs);
                mechanismUnk5 = Parser.ReadInt8(fs);
                targetId1.ReadStruct(fs);
                if (mechanismType == MechanismType.TwoTargets || (mechanismType == MechanismType.SuccessOrFailCondition))
                {
                    targetId2.ReadStruct(fs);
                }
                else
                {
                    mechanismUnk6 = Parser.ReadInt32(fs);
                }
                sound = Parser.ReadStr128(fs);
            }
            else if (triggerType == TriggerType.Lever)
            {
                operativeType = (OperativeType)Parser.ReadInt8(fs);
                leverHeldFlag = (LeverHeldFlag)Parser.ReadInt8(fs);
                leverHeldUnk1 = Parser.ReadInt8(fs);
                leverHeldUnk2 = Parser.ReadInt8(fs);
                leverHeldUnk3 = Parser.ReadInt8(fs);
                leverHeldTime = Parser.ReadInt8(fs); // Centiseconds
                leverHeldUnk4 = Parser.ReadInt8(fs);
                targetId1.ReadStruct(fs);
                if (leverHeldFlag == LeverHeldFlag.TwoTargets || (leverHeldFlag == LeverHeldFlag.TwoTargetsInverse))
                {
                    targetId2.ReadStruct(fs);
                }
                else
                {
                    leverHeldUnk5 = Parser.ReadInt32(fs);
                }
                sound = Parser.ReadStr128(fs); // Observed on level 3 secret button.
            }
            else if (triggerType == TriggerType.Button)
            {
                operativeType = (OperativeType)Parser.ReadInt8(fs);
                pressurePlateFlag = (PressurePlateFlag)Parser.ReadInt8(fs);
                plateUnk1 = Parser.ReadInt8(fs);
                plateUnk2 = Parser.ReadUInt16(fs);
                plateUnk4 = Parser.ReadInt8(fs); // Observed "1"
                plateUnk5 = Parser.ReadInt8(fs);
                targetId1.ReadStruct(fs);
                targetId2.ReadStruct(fs);
                sound = Parser.ReadStr128(fs);
            }
            else if (triggerType == TriggerType.Event)
            {
                operativeType = (OperativeType)Parser.ReadInt8(fs);
                eventFlag = Parser.ReadInt8(fs);
                eventUnk1 = Parser.ReadInt8(fs);
                eventUnk2 = Parser.ReadInt8(fs);
                eventUnk3 = Parser.ReadInt8(fs);
                eventUnk4 = Parser.ReadInt8(fs);
                eventUnk5 = Parser.ReadInt8(fs);
                if (operativeType == OperativeType.Narrator)
                {
                    narratorUnk1 = Parser.ReadInt8(fs);
                    narratorUnk2 = Parser.ReadInt8(fs);
                    narratorUnk3 = Parser.ReadInt8(fs);
                    narratorUnk4 = Parser.ReadInt8(fs);
                    targetId2.ReadStruct(fs);
                }
                else
                {
                    targetId1.ReadStruct(fs);
                    targetId2.ReadStruct(fs);
                }
                sound = Parser.ReadStr128(fs);
            }
            else if (triggerType == TriggerType.Effect)
            {
                operativeType = (OperativeType)Parser.ReadInt8(fs);
                if (operativeType == OperativeType.Fire)
                {
                    fireFlag = Parser.ReadInt8(fs); // None observed, but this may change
                    fireUnk1 = Parser.ReadInt8(fs);
                    fireSize = Parser.ReadUInt16(fs); // 0 is default size, while 1 is very small and after 500~ can cause engine issues
                    fireUnk2 = Parser.ReadInt8(fs);
                    fireUnk3 = Parser.ReadInt8(fs);
                    targetId1.ReadStruct(fs); // Unknown if fires actually trigger targets
                    targetId2.ReadStruct(fs);
                    sound = Parser.ReadStr128(fs);
                }
                else if (operativeType == OperativeType.Lightning)
                {
                    lightningFlag = Parser.ReadInt8(fs); // None observed
                    lightningAnimationType = (LightningAnimationType)Parser.ReadInt8(fs);
                    lightningOn = Parser.ReadBoolean(fs);
                    lightningUnk1 = Parser.ReadInt8(fs);
                    lightningUnk2 = Parser.ReadInt8(fs);
                    lightningUnk3 = Parser.ReadInt8(fs);
                    lightningUnk4 = Parser.ReadInt8(fs);
                    lightningWidth = Parser.ReadInt8(fs);
                    lightningLength = Parser.ReadUInt16(fs);
                    lightningIntensity = Parser.ReadUInt16(fs);
                    lightningUnk5 = Parser.ReadUInt16(fs);
                    sound = Parser.ReadStr128(fs);
                }
            }
            else if (triggerType == TriggerType.Item)
            {
                operativeType = (OperativeType)Parser.ReadInt8(fs);
                if (operativeType == OperativeType.Key)
                {
                    keyFlag = Parser.ReadInt8(fs); // No flags observed but you never know
                    keyUnk1 = Parser.ReadInt8(fs);
                    keyUnk2 = Parser.ReadInt8(fs);
                    keyUnk3 = Parser.ReadInt8(fs);
                    keyUnk4 = Parser.ReadInt8(fs);
                    keyUnk5 = Parser.ReadInt8(fs);
                    targetId1.ReadStruct(fs);
                    targetId2.ReadStruct(fs);
                    sound = Parser.ReadStr128(fs);
                }
                else if (operativeType == OperativeType.Salve)
                {
                    salveFlag = Parser.ReadInt8(fs);
                    salveUnk1 = Parser.ReadInt8(fs);
                    salveHealingTime = Parser.ReadInt8(fs); // 0 is default, 50 is observed in small salve
                    salveUnk3 = Parser.ReadInt8(fs);
                    salveUnk4 = Parser.ReadInt8(fs);
                    salveUnk5 = Parser.ReadInt8(fs);
                    targetId1.ReadStruct(fs);
                    targetId2.ReadStruct(fs);
                    sound = Parser.ReadStr128(fs);
                }
                else if (operativeType == OperativeType.LeverHeld)
                {
                    keyFlag = Parser.ReadInt8(fs); // No flags observed but you never know
                    keyUnk1 = Parser.ReadInt8(fs);
                    keyUnk2 = Parser.ReadInt8(fs);
                    keyUnk3 = Parser.ReadInt8(fs);
                    keyUnk4 = Parser.ReadInt8(fs);
                    keyUnk5 = Parser.ReadInt8(fs);
                    targetId1.ReadStruct(fs);
                    targetId2.ReadStruct(fs);
                    sound = Parser.ReadStr128(fs);
                }
                else if (operativeType == OperativeType.Elixir)
                {
                    elixirFlag = Parser.ReadInt8(fs);
                    elixirUnk1 = Parser.ReadInt8(fs);
                    elixirUnk2 = Parser.ReadInt8(fs); // Observed values
                    elixirUnk3 = Parser.ReadInt8(fs);
                    elixirUnk4 = Parser.ReadInt8(fs);
                    elixirUnk5 = Parser.ReadInt8(fs);
                    elixirUnk6 = Parser.ReadInt32(fs);
                    elixirUnk7 = Parser.ReadInt32(fs);
                    sound = Parser.ReadStr128(fs);
                }
                else if (operativeType == OperativeType.BrushScrub)
                {
                    brushScrubFlag = Parser.ReadInt8(fs);
                    brushScrubUnk1 = Parser.ReadInt8(fs);
                    brushScrubUnk2 = Parser.ReadInt8(fs);
                    brushScrubUnk3 = Parser.ReadInt8(fs);
                    brushScrubUnk4 = Parser.ReadInt8(fs);
                    brushScrubUnk5 = Parser.ReadInt8(fs);
                    brushScrubUnk6 = Parser.ReadInt32(fs);
                    brushScrubUnk7 = Parser.ReadInt32(fs);
                    sound = Parser.ReadStr128(fs);
                }
                else if (operativeType == OperativeType.RepairSphere)
                {
                    repairSphereFlag = Parser.ReadInt8(fs);
                    repairSphereUnk1 = Parser.ReadInt8(fs);
                    repairSphereUnk2 = Parser.ReadInt8(fs);
                    repairSphereUnk3 = Parser.ReadInt8(fs);
                    repairSphereUnk4 = Parser.ReadInt8(fs);
                    repairSphereUnk5 = Parser.ReadInt8(fs);
                    repairSphereUnk6 = Parser.ReadInt32(fs);
                    repairSphereUnk7 = Parser.ReadInt32(fs);
                    sound = Parser.ReadStr128(fs);
                }
            }
            else if (triggerType == TriggerType.Special)
            {
                operativeType = (OperativeType)Parser.ReadInt8(fs);
                if (operativeType == OperativeType.Compass)
                {
                    compassFlag = Parser.ReadInt8(fs);
                    compassUnk1 = Parser.ReadInt8(fs);
                    compassUnk2 = Parser.ReadInt8(fs);
                    compassUnk3 = Parser.ReadInt8(fs);
                    compassUnk4 = Parser.ReadInt8(fs);
                    compassUnk5 = Parser.ReadInt8(fs);
                    targetId1.ReadStruct(fs);
                    targetId2.ReadStruct(fs);
                    sound = Parser.ReadStr128(fs);
                }
            }
            else if (triggerType == TriggerType.Misc)
            {
                operativeType = (OperativeType)Parser.ReadInt8(fs);
                if (operativeType == OperativeType.ClickableTransition)
                {
                    clickTransitionFlag = Parser.ReadInt8(fs); // "2" observed
                    clickTransitionUnk1 = Parser.ReadInt8(fs);
                    clickTransitionUnk2 = Parser.ReadInt8(fs);
                    clickTransitionUnk3 = Parser.ReadInt8(fs);
                    clickTransitionUnk4 = Parser.ReadInt8(fs);
                    clickTransitionUnk5 = Parser.ReadInt8(fs);
                    targetId1.ReadStruct(fs); // Clickable transitions don't use target ids. Rather, they use the Thing name to determine spawning location.
                    targetId2.ReadStruct(fs);
                    sound = Parser.ReadStr128(fs); // Observed to use sounds
                }
                else if (operativeType == OperativeType.LeverHeld) // Secret switch
                {
                    leverHeldFlag = (LeverHeldFlag)Parser.ReadInt8(fs);
                    leverHeldUnk1 = Parser.ReadInt8(fs);
                    leverHeldUnk2 = Parser.ReadInt8(fs);
                    leverHeldUnk3 = Parser.ReadInt8(fs);
                    leverHeldTime = Parser.ReadInt8(fs); // Centiseconds
                    leverHeldUnk4 = Parser.ReadInt8(fs);
                    targetId1.ReadStruct(fs);
                    targetId2.ReadStruct(fs);
                    sound = Parser.ReadStr128(fs); // Observed on level 3 secret button.
                }
                else if (operativeType == OperativeType.DivinersDeck)
                {
                    divinersDeckFlag = Parser.ReadInt8(fs);
                    divinersDeckUnk1 = Parser.ReadInt8(fs);
                    divinersDeckUnk2 = Parser.ReadInt8(fs);
                    divinersDeckUnk3 = Parser.ReadInt8(fs);
                    divinersDeckUnk4 = Parser.ReadInt8(fs);
                    divinersDeckUnk5 = Parser.ReadInt8(fs);
                    divinersDeckUnk6 = Parser.ReadInt8(fs);
                    divinersDeckUnk7 = Parser.ReadInt8(fs);
                    divinersDeckUnk8 = Parser.ReadUInt16(fs);
                    divinersDeckUnk9 = Parser.ReadInt32(fs);
                    sound = Parser.ReadStr128(fs);
                }
            }
            else if (triggerType == TriggerType.Brazier)
            {
                operativeType = (OperativeType)Parser.ReadInt8(fs);
                brazierFlag = Parser.ReadInt8(fs);
                brazierUnk1 = Parser.ReadInt8(fs);
                brazierUnk2 = Parser.ReadInt32(fs);
                brazierUnk3 = Parser.ReadInt8(fs);
                brazierUnk4 = Parser.ReadInt8(fs);
                brazierUnk5 = Parser.ReadInt8(fs);
                brazierUnk6 = Parser.ReadInt8(fs);
                brazierUnk7 = Parser.ReadInt32(fs);
                sound = Parser.ReadStr128(fs);
            }

        }

        public override void WriteStruct(Stream fs)
        {
            Parser.WriteInt8(fs, (byte)triggerType);
            if (triggerType == TriggerType.Mechanism)
            {
                Parser.WriteInt8(fs, (byte)mechanismType);
                Parser.WriteInt8(fs, (byte)mechanismFlag);
                Parser.WriteInt8(fs, mechanismUnk1);
                Parser.WriteInt8(fs, mechanismUnk2);
                Parser.WriteInt8(fs, mechanismUnk3);
                Parser.WriteInt8(fs, mechanismUnk4);
                Parser.WriteInt8(fs, mechanismUnk5);
                targetId1.WriteStruct(fs);
                if (mechanismType == MechanismType.TwoTargets || (mechanismType == MechanismType.SuccessOrFailCondition))
                {
                    targetId2.WriteStruct(fs);
                }
                else
                {
                    Parser.WriteInt32(fs, mechanismUnk6);

                }
                Parser.WriteStr128(fs, sound);
            }
            else if (triggerType == TriggerType.Button)
            {
                Parser.WriteInt8(fs, (byte)operativeType);
                Parser.WriteInt8(fs, (byte)pressurePlateFlag);
                Parser.WriteInt8(fs, plateUnk1);
                Parser.WriteUInt16(fs, plateUnk2);
                Parser.WriteInt8(fs, plateUnk4);
                Parser.WriteInt8(fs, plateUnk5);
                targetId1.WriteStruct(fs);
                targetId2.WriteStruct(fs);
                Parser.WriteStr128(fs, sound);
            }
            else if (triggerType == TriggerType.Lever)
            {
                {
                    Parser.WriteInt8(fs, (byte)operativeType);
                    Parser.WriteInt8(fs, (byte)leverHeldFlag);
                    Parser.WriteInt8(fs, leverHeldUnk1);
                    Parser.WriteInt8(fs, leverHeldUnk2);
                    Parser.WriteInt8(fs, leverHeldUnk3);
                    Parser.WriteInt8(fs, leverHeldTime);
                    Parser.WriteInt8(fs, leverHeldUnk4);
                    targetId1.WriteStruct(fs);
                    if (leverHeldFlag == LeverHeldFlag.TwoTargets || (leverHeldFlag == LeverHeldFlag.TwoTargetsInverse))
                    {
                        targetId2.WriteStruct(fs);
                    }
                    else
                    {
                        Parser.WriteInt32(fs, leverHeldUnk5);
                    }
                    Parser.WriteStr128(fs, sound);
                }
            }
            else if (triggerType == TriggerType.Event)
            {
                Parser.WriteInt8(fs, (byte)operativeType);
                Parser.WriteInt8(fs, eventFlag);
                Parser.WriteInt8(fs, eventUnk1);
                Parser.WriteInt8(fs, eventUnk2);
                Parser.WriteInt8(fs, eventUnk3);
                Parser.WriteInt8(fs, eventUnk4);
                Parser.WriteInt8(fs, eventUnk5);
                if (operativeType == OperativeType.Narrator)
                {
                    Parser.WriteInt8(fs, narratorUnk1);
                    Parser.WriteInt8(fs, narratorUnk2);
                    Parser.WriteInt8(fs, narratorUnk3);
                    Parser.WriteInt8(fs, narratorUnk4);
                    targetId2.WriteStruct(fs);
                }
                else
                {
                    targetId1.WriteStruct(fs);
                    targetId2.WriteStruct(fs);
                }
                Parser.WriteStr128(fs, sound);
            }
            else if (triggerType == TriggerType.Effect)
            {
                Parser.WriteInt8(fs, (byte)operativeType);
                {
                    if (operativeType == OperativeType.Fire)
                    {
                        Parser.WriteInt8(fs, fireFlag);
                        Parser.WriteInt8(fs, fireUnk1);
                        Parser.WriteUInt16(fs, fireSize);
                        Parser.WriteInt8(fs, fireUnk2);
                        Parser.WriteInt8(fs, fireUnk3);
                        targetId1.WriteStruct(fs);
                        targetId2.WriteStruct(fs);
                        Parser.WriteStr128(fs, sound);
                    }
                    else if (operativeType == OperativeType.Lightning)
                    {
                        Parser.WriteInt8(fs, lightningFlag);
                        Parser.WriteInt8(fs, (byte)lightningAnimationType);
                        Parser.WriteBoolean(fs, lightningOn);
                        Parser.WriteInt8(fs, lightningUnk1);
                        Parser.WriteInt8(fs, lightningUnk2);
                        Parser.WriteInt8(fs, lightningUnk3);
                        Parser.WriteInt8(fs, lightningUnk4);
                        Parser.WriteInt8(fs, lightningWidth);
                        Parser.WriteUInt16(fs, lightningLength);
                        Parser.WriteUInt16(fs, lightningIntensity);
                        Parser.WriteUInt16(fs, lightningUnk5);
                        Parser.WriteStr128(fs, sound);
                    }
                }
            }
            else if (triggerType == TriggerType.Item)
            {
                Parser.WriteInt8(fs, (byte)operativeType);
                if (operativeType == OperativeType.Key)
                {
                    Parser.WriteInt8(fs, keyFlag);
                    Parser.WriteInt8(fs, keyUnk1);
                    Parser.WriteInt8(fs, keyUnk2);
                    Parser.WriteInt8(fs, keyUnk3);
                    Parser.WriteInt8(fs, keyUnk4);
                    Parser.WriteInt8(fs, keyUnk5);
                    targetId1.WriteStruct(fs);
                    targetId2.WriteStruct(fs);
                    Parser.WriteStr128(fs, sound);
                }
                else if (operativeType == OperativeType.Salve)
                {
                    Parser.WriteInt8(fs, salveFlag);
                    Parser.WriteInt8(fs, salveUnk1);
                    Parser.WriteInt8(fs, salveHealingTime);
                    Parser.WriteInt8(fs, salveUnk3);
                    Parser.WriteInt8(fs, salveUnk4);
                    Parser.WriteInt8(fs, salveUnk5);
                    targetId1.WriteStruct(fs);
                    targetId2.WriteStruct(fs);
                    Parser.WriteStr128(fs, sound);
                }
                else if (operativeType == OperativeType.LeverHeld)
                {
                    Parser.WriteInt8(fs, keyFlag);
                    Parser.WriteInt8(fs, keyUnk1);
                    Parser.WriteInt8(fs, keyUnk2);
                    Parser.WriteInt8(fs, keyUnk3);
                    Parser.WriteInt8(fs, keyUnk4);
                    Parser.WriteInt8(fs, keyUnk5);
                    targetId1.WriteStruct(fs);
                    targetId2.WriteStruct(fs);
                    Parser.WriteStr128(fs, sound);
                }
                else if (operativeType == OperativeType.Elixir)
                {
                    Parser.WriteInt8(fs, elixirFlag);
                    Parser.WriteInt8(fs, elixirUnk1);
                    Parser.WriteInt8(fs, elixirUnk2);
                    Parser.WriteInt8(fs, elixirUnk3);
                    Parser.WriteInt8(fs, elixirUnk4);
                    Parser.WriteInt8(fs, elixirUnk5);
                    Parser.WriteInt32(fs, elixirUnk6);
                    Parser.WriteInt32(fs, elixirUnk7);
                    Parser.WriteStr128(fs, sound);
                }
                else if (operativeType == OperativeType.BrushScrub)
                {
                    Parser.WriteInt8(fs, brushScrubFlag);
                    Parser.WriteInt8(fs, brushScrubUnk1);
                    Parser.WriteInt8(fs, brushScrubUnk2);
                    Parser.WriteInt8(fs, brushScrubUnk3);
                    Parser.WriteInt8(fs, brushScrubUnk4);
                    Parser.WriteInt8(fs, brushScrubUnk5);
                    Parser.WriteInt32(fs, brushScrubUnk6);
                    Parser.WriteInt32(fs, brushScrubUnk7);
                    Parser.WriteStr128(fs, sound);
                }
                else if (operativeType == OperativeType.RepairSphere)
                {
                    Parser.WriteInt8(fs, repairSphereFlag);
                    Parser.WriteInt8(fs, repairSphereUnk1);
                    Parser.WriteInt8(fs, repairSphereUnk2);
                    Parser.WriteInt8(fs, repairSphereUnk3);
                    Parser.WriteInt8(fs, repairSphereUnk4);
                    Parser.WriteInt8(fs, repairSphereUnk5);
                    Parser.WriteInt32(fs, repairSphereUnk6);
                    Parser.WriteInt32(fs, repairSphereUnk7);
                    Parser.WriteStr128(fs, sound);
                }
            }
            else if (triggerType == TriggerType.Special)
            {
                Parser.WriteInt8(fs, (byte)operativeType);
                if (operativeType == OperativeType.Compass)
                {
                    Parser.WriteInt8(fs, compassFlag);
                    Parser.WriteInt8(fs, compassUnk1);
                    Parser.WriteInt8(fs, compassUnk2);
                    Parser.WriteInt8(fs, compassUnk3);
                    Parser.WriteInt8(fs, compassUnk4);
                    Parser.WriteInt8(fs, compassUnk5);
                    targetId1.WriteStruct(fs);
                    targetId2.WriteStruct(fs);
                    Parser.WriteStr128(fs, sound);
                }
            }
            else if (triggerType == TriggerType.Misc)
            {
                Parser.WriteInt8(fs, (byte)operativeType);
                if (operativeType == OperativeType.ClickableTransition)
                {
                    Parser.WriteInt8(fs, clickTransitionFlag);
                    Parser.WriteInt8(fs, clickTransitionUnk1);
                    Parser.WriteInt8(fs, clickTransitionUnk2);
                    Parser.WriteInt8(fs, clickTransitionUnk3);
                    Parser.WriteInt8(fs, clickTransitionUnk4);
                    Parser.WriteInt8(fs, clickTransitionUnk5);
                    targetId1.WriteStruct(fs);
                    targetId2.WriteStruct(fs);
                    Parser.WriteStr128(fs, sound);
                }
                else if (operativeType == OperativeType.LeverHeld)
                {
                    Parser.WriteInt8(fs, (byte)leverHeldFlag);
                    Parser.WriteInt8(fs, leverHeldUnk1);
                    Parser.WriteInt8(fs, leverHeldUnk2);
                    Parser.WriteInt8(fs, leverHeldUnk3);
                    Parser.WriteInt8(fs, leverHeldTime);
                    Parser.WriteInt8(fs, leverHeldUnk4);
                    targetId1.WriteStruct(fs);
                    targetId2.WriteStruct(fs);
                    Parser.WriteStr128(fs, sound);
                }
                else if (operativeType == OperativeType.DivinersDeck)
                {
                    Parser.WriteInt8(fs, divinersDeckFlag);
                    Parser.WriteInt8(fs, divinersDeckUnk1);
                    Parser.WriteInt8(fs, divinersDeckUnk2);
                    Parser.WriteInt8(fs, divinersDeckUnk3);
                    Parser.WriteInt8(fs, divinersDeckUnk4);
                    Parser.WriteInt8(fs, divinersDeckUnk5);
                    Parser.WriteInt8(fs, divinersDeckUnk6);
                    Parser.WriteInt8(fs, divinersDeckUnk7);
                    Parser.WriteUInt16(fs, divinersDeckUnk8);
                    Parser.WriteInt32(fs, divinersDeckUnk9);
                    Parser.WriteStr128(fs, sound);
                }
            }
            else if (triggerType == TriggerType.Brazier)
            {
                Parser.WriteInt8(fs, (byte)operativeType);
                Parser.WriteInt8(fs, brazierFlag);
                Parser.WriteInt8(fs, brazierUnk1);
                Parser.WriteInt32(fs, brazierUnk2);
                Parser.WriteInt8(fs, brazierUnk3);
                Parser.WriteInt8(fs, brazierUnk4);
                Parser.WriteInt8(fs, brazierUnk5);
                Parser.WriteInt8(fs, brazierUnk6);
                Parser.WriteInt32(fs, brazierUnk7);
                Parser.WriteStr128(fs, sound);
            }
        }
    }
}
