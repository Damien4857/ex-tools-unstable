﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Rayform
{
	public class Apparel : HeavyChunk // 00 10 00 00
	{
		public Str128 ModelName; //Worn model from Characters.rpk.
		public Coverage[] coverages = new Coverage[100];
		public byte impactDefence;
		public byte slashDefence;
		public byte crushDefence;
		public byte pierceDefence;
		public byte coverage;
		public byte encumbrance;
		public Sound sound; //1 = Cloth, 2 = Leather, 3 = Mail, 4 = Plate
		public byte bulk;
		public ApparelType type;
		public byte grade;
		public Traits traits;
		public byte rank; //<32:Inept, <64:Aspirant, <96:Novice, <128:Adept, <160:Expert, >=160:Master
		public int rngSeed;
		public byte quality;
		public byte wear;
		public byte dirt;
		public byte variance;
		public MaterialProperties[] materials = new MaterialProperties[8];
		public Ability ability;
		public Socket[] sockets = new Socket[4];
		public Id setId = new Id();

		public enum Sound : byte
		{
			None = 0,
			Cloth = 1,
			Leather = 2,
			ChainMail = 3,
			Plate = 4,
			Unknown1 = 5,
			Unknown2 = 6,
        }
		
        public enum ApparelType : byte
        {
            None = 0,
			Clothing = 1,
			ClothingHeavy = 2,
			ClothArmour = 3,
			LeatherArmour = 4,
			MailArmour = 5,
			PlateArmour = 6
        }

		public enum Traits : byte
        {
			None = 0,
			Light = 1,
			Heavy = 2,
			Thick = 3,
			Tough = 4,
			Reinforced = 5,
			Hardened = 6,
			Blued = 7,
			Blackened = 8,
			Random = 255
        }

        public enum Ability : byte
        {
			None,
			NightVision,
			StaminaRegen,
			Healing,
			GolemSuit,
			NullMinds,
        }

        public class MaterialProperties : Struct
		{
			public byte layer;
			public byte part;
			public Element element;
			public Design design;
			public Material material;
			public byte unknown0;
			public byte unknown1;
			public Trait trait;
			public byte color;
			public byte unknown2;
			public byte unknown3;
			public Theme theme;

            public enum Design : byte
            {
				Poor = 0,
				Average = 1,
				Elegant = 2,
				Opulent = 3
            }

            public enum Material : byte
            {
				Random = 0,
				ClothRough = 1,
				ClothFine = 2,
				Silk = 3,
                Velvet = 4,
				Padded1 = 5,
				Padded2 = 6,
				Leather1 = 7,
				Leather2 = 8,
				Suede = 9,
                Rawhide = 10,
                ChainMail = 11,
                Steel = 12,
                SteelPlate = 13,
                Painted = 14,
                Bronze = 15,
                Blade = 16,
				WoodFine = 17,
                WoodRough = 18,
                Iron = 19,
				Brass = 20,
				Copper = 21,
				ClothGrip = 22,
				Gemstone = 23,
				Laquer = 24
            }

            public enum Trait : byte
            {
                Random = 0,
                None = 0
            }

            public enum Theme : byte
            {
				Random = 0,
				Main = 1,
				Accent = 2,
				Themed = 3,
				Plain = 4,
				Fixed = 5
            }

            public enum Element : byte
            {
                Random = 0,
                SoftMaterial = 1,
                HeavyMaterial = 2,
                Cloth = 3,
                Leather = 4,
                Coating = 5,
                StitchLace = 6,
                Fixture = 7,
                Buttons = 8,
                ClothArmour = 9,
                HardLeather = 10,
                Mail = 11,
                Plate = 12,
                PlateTrims = 13,
                Plain = 14,
                Normal = 15,
                Elegant = 16,
                Opulent = 17,
            }

            public override void ReadStruct(Stream fs)
			{
				layer = Parser.ReadInt8(fs);
				part = Parser.ReadInt8(fs);
				element = (Element)Parser.ReadInt8(fs);
				design = (Design)Parser.ReadInt8(fs);
				material = (Material)Parser.ReadInt8(fs);
				unknown0 = Parser.ReadInt8(fs);
				unknown1 = Parser.ReadInt8(fs);
				trait = (Trait)Parser.ReadInt8(fs);
				color = Parser.ReadInt8(fs);
				unknown2 = Parser.ReadInt8(fs);
				unknown3 = Parser.ReadInt8(fs);
				theme = (Theme)Parser.ReadInt8(fs);
			}

			public override void WriteStruct(Stream fs)
			{
				Parser.WriteInt8(fs, layer);
				Parser.WriteInt8(fs, part);
				Parser.WriteInt8(fs, (byte)element);
				Parser.WriteInt8(fs, (byte)design);
				Parser.WriteInt8(fs, (byte)material);
				Parser.WriteInt8(fs, unknown0);
				Parser.WriteInt8(fs, unknown1);
				Parser.WriteInt8(fs, (byte)trait);
				Parser.WriteInt8(fs, color);
				Parser.WriteInt8(fs, unknown2);
				Parser.WriteInt8(fs, unknown3);
				Parser.WriteInt8(fs, (byte)theme);
			}
		}
		
		public class Coverage
		{
			public Int4[] value = new Int4[8]; //slot covering
		}

		// TODO: Shouldn't this use StartReadChunk or something?
		public override void ReadStruct(Stream fs)
		{
			ModelName = Parser.ReadStr128(fs);

			for (int i = 0; i < coverages.Length; i++)
			{
				Coverage coverage = new Coverage();
				coverage.value = Parser.ReadInt4(fs, 4);
				coverages[i] = coverage;
			}

			impactDefence = Parser.ReadInt8(fs);
			slashDefence = Parser.ReadInt8(fs);
			crushDefence = Parser.ReadInt8(fs);
			pierceDefence = Parser.ReadInt8(fs);
			coverage = Parser.ReadInt8(fs);
			encumbrance = Parser.ReadInt8(fs);
			sound = (Sound)Parser.ReadInt8(fs);
			bulk = Parser.ReadInt8(fs);
			type = (ApparelType)Parser.ReadInt8(fs);
			grade = Parser.ReadInt8(fs);
			traits = (Traits)Parser.ReadInt8(fs);
			rank = Parser.ReadInt8(fs);
			rngSeed = Parser.ReadInt32(fs);
			quality = Parser.ReadInt8(fs);
			wear = Parser.ReadInt8(fs);
			dirt = Parser.ReadInt8(fs);
			variance = Parser.ReadInt8(fs);

			for (int i = 0; i < materials.Length; i++)
			{
				MaterialProperties material = new MaterialProperties();
				material.ReadStruct(fs);
				materials[i] = material;
			}

			ability = (Ability)Parser.ReadInt32(fs);

			for (int i = 0; i < sockets.Length; i++)
			{
				Socket socket = new Socket();
				socket.ReadStruct(fs);
				sockets[i] = socket;
			}
			setId.ReadStruct(fs);
        }

        public override void WriteStruct(Stream fs)
        {
			Parser.WriteStr128(fs, ModelName);

            foreach (Coverage coverage in coverages)
            {
				Parser.WriteInt4(fs, coverage.value);
            }

			Parser.WriteInt8(fs, impactDefence);
			Parser.WriteInt8(fs, slashDefence);
			Parser.WriteInt8(fs, crushDefence);
			Parser.WriteInt8(fs, pierceDefence);
			Parser.WriteInt8(fs, coverage);
			Parser.WriteInt8(fs, encumbrance);
			Parser.WriteInt8(fs, (byte)sound);
			Parser.WriteInt8(fs, bulk);
			Parser.WriteInt8(fs, (byte)type);
			Parser.WriteInt8(fs, grade);
			Parser.WriteInt8(fs, (byte)traits);
			Parser.WriteInt8(fs, rank);
			Parser.WriteInt32(fs, rngSeed);
			Parser.WriteInt8(fs, quality);
			Parser.WriteInt8(fs, wear);
			Parser.WriteInt8(fs, dirt);
			Parser.WriteInt8(fs, variance);

            foreach (MaterialProperties material in materials)
            {
                material.WriteStruct(fs);
            }

            Parser.WriteInt32(fs, (int)ability);

            foreach (Socket socket in sockets)
            {
                socket.WriteStruct(fs);
            }

            setId.WriteStruct(fs);
        }
	}
}
