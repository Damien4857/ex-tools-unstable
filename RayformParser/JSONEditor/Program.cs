﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Rayform;

class Program
{
    static void Main(string[] args)
    {
        JsonConvert.DefaultSettings = () => new JsonSerializerSettings()
        {
            //Converters = new List<Newtonsoft.Json.JsonConverter>() { new Newtonsoft.Json.Converters.StringEnumConverter() },
            TypeNameHandling = TypeNameHandling.Auto,
            SerializationBinder = new RFBinder()
        };
        //args = new string[]{ "C:\\Users\\utente\\Desktop\\ExTools\\RayformParser\\JSONEditor\\bin\\Debug\\net6.0\\Exanima006.json" };

        foreach (string fileName in args)
        {
            if(Path.GetExtension(fileName) != ".json")
            {
                OpenFile openFile = OpenFile.Load(fileName);

                Chunk chunk = Chunk.StartReadChunk(openFile.fs);
                if (chunk.GetType() != typeof(UnknownChunk))
                {
                    openFile.data = chunk;
                    chunk.ReadStruct(openFile.fs);
                }
                else
                {
                    openFile.fs.Position = 0;
                    string extension = Path.GetExtension(fileName);
                    if (extension == ".rsg")
                    {
                        openFile.data = new Save();
                        openFile.data.ReadStruct(openFile.fs);
                    }
                    else if (extension == ".rfc")
                    {
                        openFile.data = new Content();
                        openFile.data.ReadStruct(openFile.fs);
                    }
                }

                var jsonObject = new
                {
                    Type = openFile.data.GetType().ToString(),
                    Data = openFile.data
                };

                string json = JsonConvert.SerializeObject(jsonObject, Formatting.Indented);
                File.WriteAllText(fileName + ".json", json);
            }
            else
            {
                OpenFile newFile = OpenFile.Load(Path.GetDirectoryName(fileName) + '\\' + Path.GetFileNameWithoutExtension(fileName));
                dynamic jsonObject = JObject.Parse(File.ReadAllText(fileName));
                Type type = Type.GetType(jsonObject.Type.ToString());
                newFile.data = (Struct)JsonConvert.DeserializeObject(jsonObject.Data.ToString(), type);

                if(newFile.data.GetType().BaseType == typeof(Chunk))
                {
                    ((Chunk)newFile.data).WriteChunk(newFile.fs);
                }
                else
                {
                    newFile.data.WriteStruct(newFile.fs);
                }
                
                newFile.fs.Flush();
            }
        }
        Console.ReadKey();
    }
}