using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

public class LibNodeSceneHandler : MonoBehaviour
{
    public static Toolbar.LibNodeData[] CollectLibNodeData()
    {
        List<Toolbar.LibNodeData> libNodeDataList = new List<Toolbar.LibNodeData>();

        GameObject[] selectedObjects = Selection.gameObjects;

        foreach (GameObject selectedObject in selectedObjects)
        {
            if (selectedObject.GetComponent<LibNode>() != null)
            {
                Toolbar.LibNodeData data = new Toolbar.LibNodeData();
                data.modelName = selectedObject.name;
                data.position = selectedObject.transform.position;
                data.rotation = selectedObject.transform.rotation.eulerAngles;
                data.scale = selectedObject.transform.localScale;

                libNodeDataList.Add(data);
            }
        }

        return libNodeDataList.ToArray();
    }
}
