using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

partial class ExanimaResources
{
    public static Material ImportMaterial(Rayform.Material RFmaterial)
    {
        Material material = materials.FirstOrDefault(x => x.name == RFmaterial.name);
        if (material != null) return null;

        material = new UnityEngine.Material(Shader.Find("Standard (Specular setup)"));
        material.name = RFmaterial.name;

        //material.EnableKeyword("_NORMALMAP");
        if (RFmaterial.diffuse != null)
        {
            ImportedAsset diffuseAsset = ExanimaResources.LoadByName(RFmaterial.diffuse.name, typeof(Texture2D), Rayform.PackageManager.PackageCategory.Textures);
            if (diffuseAsset != null)
            {
                Texture2D texture = (Texture2D)diffuseAsset._object;
                material.mainTexture = texture;
                if (texture.alphaIsTransparency && !new string[] { "rock", "trnrck02cv" }.Contains(RFmaterial.name.ToString()))
                {
                    material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                    material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
                    material.SetInt("_ZWrite", 1);
                    material.EnableKeyword("_ALPHATEST_ON");
                    material.DisableKeyword("_ALPHABLEND_ON");
                    material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                    material.renderQueue = 2450;
                }
            }
        }
        //if (RFmaterial.bump != null)
        //{
        //    ImportedAsset bumpAsset = ExanimaResources.LoadFromRpk(RFmaterial.bump.name, typeof(Texture2D));
        //    if (bumpAsset != null)
        //    {
        //        material.SetTexture("_BumpMap", (Texture2D)bumpAsset._object);
        //        material.EnableKeyword("_NORMALMAP");
        //    }
        //
        //}

        materials.Add(material);
        return material;
    }
}
