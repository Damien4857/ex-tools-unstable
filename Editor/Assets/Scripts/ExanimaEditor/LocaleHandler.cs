using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Claims;
using TreeEditor;
using UnityEditor;
using UnityEngine;
using static Rayform.Tilemap.Cell;

public partial class ExanimaEditor
{
    //public static string[] walls = new string[] { "null", "+WLF", "+WRF", "null", "null", "null", "null", "+WCA", "null", "null", "null", "+WDF", "null", "null", "null", "null" };
    //public static string[] corners = new string[] { "null", "+CLA", "+CRA", "+CIA", "+COA", "null", "unk", "unk", "floor?", "+CLA", "+CRA", "+CIA", "+COA", "unk", "unk", "unk" };
    //public static string[] floors = new string[] { "+FMA", "unk", "unk", "unk", "unk", "unk", "unk", "unk", "unk", "unk", "unk", "unk", "unk", "unk", "unk", "null" };
    public static Rayform.Tilemap tilemap;
    public static List<ImportedAsset> wallSetAssets;
    public static List<ImportedAsset> floorSetAssets;

    public static List<Material> floorMaterials;

    // Why use chunks if you still use them in a set order? Madoc moment
    public static Type[] chunkOrder = new Type[] { typeof(Rayform.Tilemap), typeof(Rayform.Materials), typeof(Rayform.Scene), typeof(Rayform.LibNodes), typeof(Rayform.LocaleObjs), typeof(Rayform.LocaleChars), typeof(Rayform.Enviroment) };

    public static GameObject ImportContent(Rayform.Content locale, string name)
    {
        Rayform.Save save = null;

        if (file.data.GetType() == typeof(Rayform.Save))
        {
            save = (Rayform.Save)file.data;
        }

        GameObject localeGroup = new GameObject(name);
        Tag.Add(localeGroup, name);

        //ImportedAsset test = ExanimaResources.LoadFromRpk("hrta clst 03 c", typeof(GameObject));
        //return null;
        //human.transform.parent = localeGroup.transform;



        //import all assets
        Rayform.LibNodes libNodes = locale.chunks.OfType<Rayform.LibNodes>().FirstOrDefault();

        if (libNodes != null)
        {
            GameObject libNodesGroup = new GameObject("LibNodes");
            Tag.Add(libNodesGroup, "LibNodes");
            libNodesGroup.transform.parent = localeGroup.transform;
            //import nodes
            for (int i = 0; i < libNodes.nodes.Count; i++)
            {
                GameObject nodeObj = LibNode.Import(libNodes.nodes[i]);
            }
        }
        // import objects (items, doors, containers, zones)

        //read the materials
        Rayform.Materials RFmaterials = locale.chunks.OfType<Rayform.Materials>().FirstOrDefault();

        if (RFmaterials != null)
        {
            foreach (Rayform.Material RFmaterial in RFmaterials.materials)
            {
                ExanimaResources.ImportMaterial(RFmaterial);
            }
        }

        Rayform.LocaleObjs localeObjs = locale.chunks.OfType<Rayform.LocaleObjs>().FirstOrDefault();
        if (localeObjs != null)
        {
            GameObject objectsGroup = new GameObject("Objects");
            Tag.Add(objectsGroup, "Objects");
            objectsGroup.transform.parent = localeGroup.transform;

            Rayform.BaseObjs baseObjs = localeObjs.chunks.OfType<Rayform.BaseObjs>().FirstOrDefault();
            foreach (Rayform.BaseObjs.Node baseObj in baseObjs.nodes)
            {
                GameObject objectObj = Object.Import(baseObj);
            }
        }

        GameObject CharactersGroup = new GameObject("Characters");
        Tag.Add(CharactersGroup, "Characters");
        CharactersGroup.transform.parent = localeGroup.transform;
        Rayform.LocaleChars localeChars = locale.chunks.OfType<Rayform.LocaleChars>().FirstOrDefault();
        if (localeChars != null)
        {
            foreach (Rayform.CharacterPosition characterPosition in localeChars.chunks.OfType<Rayform.CharacterPosition>())
            {
                GameObject characterObj = Character.Import(characterPosition);
            }
        }
        else
        {
            localeChars = new Rayform.LocaleChars();
            localeChars.chunks.Add(new Rayform.RdbChars());
            locale.chunks.Add(localeChars);
        }

        Rayform.Scene scene = locale.chunks.OfType<Rayform.Scene>().FirstOrDefault();
        GameObject sceneObj = null;
        if (scene != null)
        {
            sceneObj = ExanimaEditor.ImportScene(scene, "Scene");

        }
        else
        {
            sceneObj = new GameObject("Scene");
        }
        Tag.Add(sceneObj, "Scene");
        sceneObj.transform.parent = localeGroup.transform;


        tilemap = locale.chunks.OfType<Rayform.Tilemap>().FirstOrDefault();
        if(tilemap != null)
        {
            ImportTilemap(tilemap);
        }

        // import enviroment
        Rayform.Enviroment enviroment = locale.chunks.OfType<Rayform.Enviroment>().FirstOrDefault();
        if (enviroment != null)
        {
            ImportEnviroment(enviroment);
        }
        else
        {
            locale.chunks.Add(new Rayform.Enviroment());
        }
        return localeGroup;
    }

    public static void ImportTilemap(Rayform.Tilemap tilemap)
    {
        floorMaterials = new List<Material>();

        GameObject tilemapGroup = Tag.Find("Tilemap");
        if(tilemapGroup != null)
        {
            GameObject.DestroyImmediate(tilemapGroup);
        }

        tilemapGroup = new GameObject("Tilemap");
        Tag.Add(tilemapGroup, "Tilemap");
        SceneVisibilityManager.instance.DisablePicking(tilemapGroup, true);
        tilemapGroup.transform.parent = Tag.Find(selLocale).transform;

        int gridSize = tilemap.gridSize / 100;

        wallSetAssets = new List<ImportedAsset>();
        for (int i = 0; i < tilemap.wallSets.Count; i++)
        {
            string wallset = ((string)tilemap.wallSets[i]) + ".rfc";
            ImportedAsset tileSetAsset = ExanimaResources.LoadByName(wallset, typeof(GameObject), Rayform.PackageManager.PackageCategory.Resources);
            wallSetAssets.Add(tileSetAsset);
            //tilemapAsset.Instantiate();
        }

        floorSetAssets = new List<ImportedAsset>();
        for (int i = 0; i < tilemap.floorSets.Count; i++)
        {
            string wallset = ((string)tilemap.floorSets[i]) + ".rfc";
            ImportedAsset floorSetAsset = ExanimaResources.LoadByName(wallset, typeof(GameObject), Rayform.PackageManager.PackageCategory.Resources);
            floorSetAssets.Add(floorSetAsset);
            //tilemapAsset.Instantiate();
        }

        for (int y = 0; y < tilemap.gridHeight; y++)
        {
            for (int x = 0; x < tilemap.gridWidth; x++)
            {
                ImportCell(x, y);
            }
        }
        SceneVisibilityManager.instance.DisablePicking(tilemapGroup, true);
    }

    public static void ImportCell(int x, int y)
    {
        GameObject tilemapGroup = Tag.Find("Tilemap");
        Transform check = tilemapGroup.transform.Find("cell " + x + " " + y);
        if (check != null)
        {
            GameObject.DestroyImmediate(check.gameObject);
        }

        Rayform.Tilemap.Cell cell = tilemap.cells[x, y];

        //if the tile is empty, skip
        //if (cell.tilesetsEnabled)
        //{
        //    return;
        //}

        GameObject cellObj = new GameObject("cell " + x + " " + y);
        int gridSize = tilemap.gridSize / 100;
        Vector3 tilemapPosition = new Vector3((tilemap.gridWidth - 1) * gridSize / 2, 0, (tilemap.gridHeight - 1) * -gridSize / 2);

        cellObj.transform.parent = tilemapGroup.transform;
        cellObj.transform.position = new Vector3(-x * gridSize, 0, y * gridSize) + tilemapPosition;

        Texture floorTex = null;

        //Debug.Log(cell.floorSet);
        if (cell.floorSet < floorSetAssets.Count)
        floorTex = ((GameObject)floorSetAssets[cell.floorSet]._object).GetComponentInChildren<MeshRenderer>().sharedMaterial.mainTexture;

        // Floor
        //if(cell. < floorSetAssets.Count)
        //{
        //    GameObject floorObj = GameObject.CreatePrimitive(PrimitiveType.Plane);
        //    floorObj.name = "+Floor";
        //    floorObj.transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
        //    floorObj.transform.parent = cellObj.transform;
        //    floorObj.transform.localPosition = new Vector3(0, 0.0005f, 0);
        //    floorObj.GetComponent<MeshRenderer>().material = ((GameObject)floorSetAssets[tileT.w]._object).GetComponentInChildren<MeshRenderer>().sharedMaterial;
        //}

        // Terrain
        if (cell.terrainEnabled)
        {
        //    GameObject floorObj = GameObject.CreatePrimitive(PrimitiveType.Plane);
        //    floorObj.name = "+Floor";
        //    floorObj.transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
        //    floorObj.transform.parent = cellObj.transform;
        //    floorObj.transform.localPosition = new Vector3(0, 0.0005f, 0);
        //    //floorObj.GetComponent<MeshRenderer>().material = ((GameObject)floorSetAssets[tileT.w]._object).GetComponentInChildren<MeshRenderer>().sharedMaterial;
        }

        //if (tileT.w != 15)
        //{
        //    GameObject ceilingObj = GameObject.CreatePrimitive(PrimitiveType.Plane);
        //    ceilingObj.name = "Ceiling";
        //    ceilingObj.transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
        //    ceilingObj.transform.parent = cellObj.transform;
        //    ceilingObj.transform.localPosition = new Vector3(0, 4, 0);
        //    ceilingObj.GetComponent<MeshRenderer>().material = ExanimaResources.materials.FirstOrDefault(x => x.name == "black");
        //}

        TileType[] types = cell.GetActiveTiles();
        foreach (TileType type in types)
        {
            Vector3 rotation = new Vector3(0, cell.getRotation(type), 0);
            GroupSet groupSet = cell.GetGroups(type);
            int variant = cell.GetVariant(type);

            Transform tileAsset = null;

            //Debug.Log("variant " + variant);

            if (cell.wallSet < wallSetAssets.Count)
            {
                foreach (string[] groups in groupSet.walls)
                {
                    //Debug.Log(groups[0]);
                    var variantList = ((GameObject)wallSetAssets[cell.wallSet]._object).transform.Children().Where(x => groups.Any(t => x.name.StartsWith(t))).OrderBy(x => x.name.Substring(4,2)).ToList();
                    if (variantList.Any())
                    {
                        //Debug.Log(variantList[0]);
                        if(variant >= variantList.Count)
                        variant = 0;
                        tileAsset = variantList[variant];
                        ImportTile(tileAsset, cellObj, rotation, floorTex, cell);
                    }
                }
            }
            if (cell.floorSet < floorSetAssets.Count)
            {
                foreach (string[] groups in groupSet.floors)
                {
                    //Debug.Log(groups[0]);
                    var variantList = ((GameObject)floorSetAssets[cell.floorSet]._object).transform.Children().Where(x => groups.Any(t => x.name.StartsWith(t))).OrderBy(x => x.name.Substring(4, 2)).ToList();
                    if (variantList.Any())
                    {
                        //Debug.Log(variantList[0]);
                        if (variant >= variantList.Count)
                        variant = 0;

                        tileAsset = variantList[variant];
                        ImportTile(tileAsset, cellObj, rotation, floorTex, cell);
                    }
                }
            }
            

            //if (tileAsset == null)
            //{ 
            //    foreach (ImportedAsset tilesetAsset in tileSetAssets)
            //    {
            //        tileAsset = ((GameObject)tilesetAsset._object).transform.Children().FirstOrDefault(x => x.name.StartsWith(tile));
            //    }
            //}

            else
            {
                //Debug.Log(groups);
            }
        }
    }

    public static void ImportTile(Transform tileAsset, GameObject cellObj, Vector3 rotation, Texture floorTex, Rayform.Tilemap.Cell cell)
    {
        if (tileAsset != null)
        {
            GameObject tileObj = GameObject.Instantiate(tileAsset.gameObject);
            tileObj.name = tileAsset.name;

            tileObj.transform.parent = cellObj.transform;
            tileObj.transform.localPosition = Vector3.zero;
            tileObj.transform.eulerAngles = rotation;

            foreach (MeshRenderer floorRenderer in tileObj.GetComponentsInChildren<MeshRenderer>())
            {
                if(floorRenderer && floorRenderer.name.StartsWith('+'))
                {
                    int index = Array.IndexOf(floorRenderer.sharedMaterials, floorRenderer.sharedMaterials.Where(x => x.name.StartsWith("+")).FirstOrDefault());
                    if (index != -1)
                    {
                        if (cell.floorSet != 15)
                        {
                            var materials = floorRenderer.sharedMaterials;

                            Material floorMaterial = floorMaterials.FirstOrDefault(x => x.mainTexture == floorTex);
                            if (floorMaterial == null)
                            {
                                floorMaterial = new Material(Shader.Find("Exanima/Floor"));
                                floorMaterials.Add(floorMaterial);
                            }
                            materials[index] = floorMaterial;

                            materials[index].mainTexture = floorTex;
                            floorRenderer.sharedMaterials = materials;
                        }
                        else
                        {
                            GameObject.DestroyImmediate(floorRenderer.gameObject);
                        }
                    }
                }
            }
        }
    }

    public static void ImportEnviroment(Rayform.Enviroment enviroment)
    {
        //RenderSettings.ambientLight = enviroment.ambient.ConvertColor();
    }

    public static void ExportLocale(Rayform.Content locale, string name)
    {
        GameObject localeGroup = Tag.Find(name);

        // Export libnodes
        Rayform.LibNodes libNodes = locale.chunks.OfType<Rayform.LibNodes>().FirstOrDefault();
        libNodes.nodes.Clear();

        foreach (LibNode nodeObj in GameObject.FindObjectsOfType<LibNode>().OrderBy(m => m.transform.GetSiblingIndex()).ToArray())
        {
            libNodes.nodes.Add(nodeObj.Export());
			//Debug.Log(nodeObj);
        }

        // Export objects
        Rayform.LocaleObjs localeObjs = locale.chunks.OfType<Rayform.LocaleObjs>().FirstOrDefault();
        if(localeObjs != null)
        {
            Rayform.BaseObjs baseObjs = localeObjs.chunks.OfType<Rayform.BaseObjs>().FirstOrDefault();
            baseObjs.nodes.Clear();

            foreach (Object objectObj in GameObject.FindObjectsOfType<Object>().OrderBy(m => m.transform.GetSiblingIndex()).ToArray())
            {
                Rayform.BaseObjs.Node node = objectObj.Export();
                baseObjs.nodes.Add(node);
            }
        }

        Rayform.LocaleChars localeChars = locale.chunks.OfType<Rayform.LocaleChars>().FirstOrDefault();
        if(localeChars != null)
        {
            localeChars.chunks.RemoveAll(obj => obj.GetType() == typeof(Rayform.CharacterPosition));

            foreach (Character characterObj in GameObject.FindObjectsOfType<Character>().OrderBy(m => m.transform.GetSiblingIndex()).ToArray())
            {
                Rayform.CharacterPosition characterPosition = characterObj.Export();
                if(characterObj.tag != "Player")
                {
                    localeChars.chunks.Add(characterPosition);
                }
            }
        }

        Rayform.Scene scene = locale.chunks.OfType<Rayform.Scene>().FirstOrDefault();

        // Remember to remove this after i add support for more node types
        List<Rayform.Scene.Node> tempFix = new List<Rayform.Scene.Node>();
		
		
		if (scene != null)
		{
		    tempFix = scene.nodes;
		    tempFix.RemoveAll(x => x.GetType() == typeof(Rayform.LightSource));
		    tempFix.RemoveAll(x => x.GetType() == typeof(Rayform.VoxelLight));
		    locale.chunks.Remove(scene);
		}

        GameObject sceneObj = Tag.Find("Scene"); 
        if(sceneObj != null)
        {
            scene = ExportScene(sceneObj);
            locale.chunks.Add(scene);
        }

        // tempFix fix
        scene.nodes = scene.nodes.Concat(tempFix).ToList();

        Rayform.Enviroment enviroment = locale.chunks.OfType<Rayform.Enviroment>().FirstOrDefault();
        if(enviroment!= null)
        {
            //enviroment.ambient = RenderSettings.ambientLight.ConvertColor();
            //enviroment.sunlightColor = RenderSettings.ambientLight.ConvertColor();
        }

        // Chunk order is essential or everything may break
        locale.chunks = locale.chunks.OrderBy(obj => Array.IndexOf(chunkOrder, obj.GetType())).ToList();
    }

    public static void ImportSubnodes(Rayform.Subnodes subnodes, GameObject obj)
    {
        Transform transform = obj.transform.GetChild(0);
        transform.Reset();
        Transform[] children = GetChildTransformsRecursively(transform);

        for (int i=0; i < children.Length; i++)
        {
            if (i < subnodes?.nodes.Count)
            {
                children[i].FromMatrix(subnodes.nodes[i].transform.ConvertTo4x4(), false);
            }
            else
            {
                //children[i].Reset();
            }
            //if (node != null)
            //{
            //    child.FromMatrix(node.transform.ConvertTo4x4(), false);
            //}

            //else
            //{
            //    
            //}
        }
    }

    public static void ImportSubnodes2(Rayform.Subnodes2 subnodes, GameObject obj)
    {
        Transform transform = obj.transform.GetChild(0);
        transform.Reset();
		//obj.transform.FromMatrix(subnodes.transform.ConvertTo4x4(), false);
        Transform[] children = GetChildTransformsRecursively(transform);

		children[0].FromMatrix(subnodes.transform.ConvertTo4x4(), false);
        for (int i=1; i < children.Length; i++)
        {
            if (i < subnodes?.nodes.Count)
            {
                children[i].FromMatrix(subnodes.nodes[i-1].transform.ConvertTo4x4(), false);
            }
            else
            {
                //children[i].Reset();
            }
            //if (node != null)
            //{
            //    child.FromMatrix(node.transform.ConvertTo4x4(), false);
            //}

            //else
            //{
            //    
            //}
        }
    }
	
	
    public static Rayform.Subnodes ExportSubnodes(GameObject obj)
    {
        Transform transform = obj.transform.GetChild(0);
        Transform[] children = GetChildTransformsRecursively(transform);

        Rayform.Subnodes subnodes = new Rayform.Subnodes();
        // Export subnodes, the first gameobject is the main mesh so skip
        foreach (Transform child in children)
        {
            Rayform.Subnodes.Node node = new Rayform.Subnodes.Node();
            node.transform = child.transform.ToMatrix(false).ConvertTo3x4();
            node.name = child.name;
            node.children = child.childCount;
            node.totalChildren = GetChildTransformsRecursively(child).Length;
            subnodes.nodes.Add(node);
        }
        //subnodes.unknown = Math.Clamp(subnodes.nodes.Count, 0, 2);
        subnodes.children = transform.childCount;
        return subnodes;
    }

    public static Transform[] GetChildTransformsRecursively(Transform parentTransform)
    {
        List<Transform> childTransforms = new List<Transform>();
        foreach (Transform childTransform in parentTransform)
        {
            childTransforms.Add(childTransform);
            childTransforms.AddRange(GetChildTransformsRecursively(childTransform));
        }
        return childTransforms.ToArray();
    }
}
