using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO.Ports;
using System.Linq;
using UnityEditor;
using UnityEngine;
using static System.Net.Mime.MediaTypeNames;

[ExecuteInEditMode]
[SelectionBase]
[GlobalSelectionBase]
public class Object : MonoBehaviour
{
    public Rayform.Id id;
    public Rayform.ObjClass.Flags flags;
    public Rayform.ObjClass data;
    public GameObject model;
    [SerializeField]
    public bool exportSubnodes;
    //public bool exportSubnodes;

    // Ty Jangkgdo, I will not eat your skin today
    public static Dictionary<string, string> replacementDic = new Dictionary<string, string> {
    {"@mace_knbda1", "ub5 mace1ha"},
    {"@halberd a1", "halberd a"},
    {"@sickle2h_a1", "billhooka"},
    {"@sickle1h_a1", "billhooka"},
    {"@macespiked a1", "ub5 mace1ha"},
    {"@axebtl1h_b1", "ub5 axe1ha"},
    {"@hrshammer_a1", "hammer"},
    {"@sword_lnga1", "sword_proctorlh"},
    {"@cudgel_a1", "cudgle01"},
    {"@metalbar_a1", "metalbarrusty a"},
    {"@cleaver_a1", "knife_brass a01"},
    {"@hammer_tol_a01", "ub5 hammer1ha"},
    {"@maul2h_a01", "maul a"},
    {"@axetool2h_a01", "ub5 axe1ha"},
    {"@billtool2h_a1", "billhooka"},
    {"@sword_arma1", "sword_proctorlh"},
    {"@warhammer_a1", "hammer03"},
    {"@spear_lnga1", "ub5 speara"},
    {"@falchiona1", "falchiona"},
    {"@hrsaxe_a1", "ub5 axe1ha"},
    {"@axebtl2h_b1", "ub5 axe1ha"},
    {"@axe btl 2h a1", "ub5 axe1ha"},
    {"@quarterstaff_b1", "metalbarrusty a"},
    {"@falchion2ha1", "falchionb"},
    {"@bardiche_a1", "ub5 axe1ha"},
    {"@sword_shra1", "sword_proctorlh"},
    {"@macespiked b1", "ub5 mace1ha"},
    {"@hammer_smt_a1", "hammer04"},
    {"@hammer_sld_a01", "hammer05"},
    {"@sawhand_a1", "knife_brass a01"},
    {"@knifelong_a1", "knife_brass a01"},
    {"@mace_flnga1", "ub5 mace1ha"},
    {"@spear_shrta1", "ub5 speara"},
    {"@sword_2ha1", "sword2hd"},
    {"@warhammer 2h a1", "hammer05"},
    {"@billtool1h_a1", "billhooka"},
    {"@mace flng 2h a1", "ub5 mace1ha"},
    {"@daggera1", "dagger b01"},
    {"@axebtl1h_a1", "ub5 axe1ha"},
    {"@macespiked a2", "ub5 mace1ha"},
    {"@voulge_a01", "poleaxea"},
    //{"@voulge_a01", "KrgMess01"}
};

    public static GameObject Import(Rayform.BaseObjs.Node baseObj)
    {
        GameObject objectObj = Import(baseObj.id);
        if (objectObj == null) return null;

        objectObj.transform.FromMatrix(baseObj.transform.ConvertTo4x4(), false);
        Object objectComp = objectObj.GetComponent<Object>();
        objectComp.flags = baseObj.flags;
        //if (baseObj.subnodes != null && baseObj.subnodes.nodes.Count > 0)
        //{
        //    objectComp.exportSubnodes = true;
        //}

        if (objectComp.model != null)
        {
            if (baseObj.subnodes?.nodes.Count > 0)
            {
                objectComp.exportSubnodes = true;
            }

            ExanimaEditor.ImportSubnodes(baseObj.subnodes, objectComp.model);
        }
		if (baseObj.subnodes2 is not null)
		{
			objectComp.exportSubnodes = true;
			ExanimaEditor.ImportSubnodes2(baseObj.subnodes2, objectComp.model);
		}

        return objectObj;

    }

    public static GameObject Import(Rayform.Id id)
    {
        Rayform.Save save = null;
        Rayform.Content locale = null;
        if (ExanimaEditor.file.data.GetType() == typeof(Rayform.Save))
        {
            save = (Rayform.Save)ExanimaEditor.file.data;
            locale = (Rayform.Content)save.sections.FirstOrDefault(x => x.name == ExanimaEditor.selLocale).data;
        }
        else
        {
            locale = (Rayform.Content)ExanimaEditor.file.data;
        }
        Rayform.ObjClass objClass = (Rayform.ObjClass)Rayform.Rdb.FindRdbEntry(locale, save, id, typeof(Rayform.RdbObjs));
        if (objClass == null) return null;

        Rayform.Thing thing = objClass.classes.OfType<Rayform.Thing>().FirstOrDefault();
        if (thing == null) return null;

        GameObject objectObj = new GameObject();
        objectObj.name = thing.thingName;
        objectObj.transform.parent = Tag.Find("Objects").transform;
        Object objectComp = objectObj.AddComponent<Object>();
        objectComp.data = objClass;
        objectComp.id = id;

        Rayform.Object obj = objClass.classes.OfType<Rayform.Object>().FirstOrDefault();

        if (obj != null)
        {
            string modelName = obj.modelName;
            // Havent figured out factories yet
            if (replacementDic.TryGetValue(modelName.ToLower(), out string replacementModel))
            {
                modelName = replacementModel;
            }

            ImportedAsset asset = ExanimaResources.LoadByPath("Objects.rpk\\" + modelName, typeof(GameObject));
            if (asset.data == null)
            {
                asset = ExanimaResources.LoadByPath("Objects.rpk\\DropTable", typeof(GameObject));
            }

            if (asset.data != null)
            {
                objectComp.model = asset.Instantiate();
                objectComp.model.transform.parent = objectObj.transform;
                objectComp.model.transform.Reset();
                objectComp.model.transform.GetChild(0).Reset();
            }
        }

        return objectObj;
    }

    public Rayform.BaseObjs.Node Export()
    {
        Rayform.BaseObjs.Node node = new Rayform.BaseObjs.Node();
        node.id = id;
        node.flags = flags;
        node.transform = Utils.ConvertTo3x4(transform.ToMatrix(false));

        if (exportSubnodes && model != null)
        {
            node.subnodes = ExanimaEditor.ExportSubnodes(model.gameObject);
        }
        else
        {
            node.subnodes = new Rayform.Subnodes();
        }

        return node;
    }

    private void Awake()
    {
        if (data == null && id.baseId != 0)
        {
            Rayform.Save save = null;
            Rayform.Content locale = null;
            if (ExanimaEditor.file.data.GetType() == typeof(Rayform.Save))
            {
                save = (Rayform.Save)ExanimaEditor.file.data;
                locale = (Rayform.Content)save.sections.FirstOrDefault(x => x.name == ExanimaEditor.selLocale).data;
            }
            else
            {
                locale = (Rayform.Content)ExanimaEditor.file.data;
            }

            data = (Rayform.ObjClass)Rayform.Rdb.FindRdbEntry(locale, save, id, typeof(Rayform.RdbObjs));
            name = data.classes.OfType<Rayform.Thing>().FirstOrDefault().thingName;
        }
    }

    [CustomEditor(typeof(Object))]
    public class CustomInspector : Editor
    {
        Object target;

        public override void OnInspectorGUI()
        {
            GUIStyle style = new GUIStyle { richText = true };
            target.exportSubnodes = EditorGUILayout.Toggle("Export Subnodes", target.exportSubnodes);

            GUILayout.BeginVertical(EditorStyles.helpBox);
            target.id.baseId = (ushort)EditorGUILayout.IntField("Base Id", target.id.baseId);
            target.id.flags = (Rayform.Id.Flags)EditorGUILayout.EnumPopup("Flags", target.id.flags);
            GUILayout.EndVertical();

            //target.exportSubnodes = EditorGUILayout.Toggle("Export SubNodes", target.exportSubnodes);
            target.flags = (Rayform.ObjClass.Flags)EditorGUILayout.EnumFlagsField("Flags", target.flags);

            Object.ObjectFields(target.data);
        }

        void Awake()
        {
            target = (Object)serializedObject.targetObject;
        }

        private void OnSceneGUI()
        {
            Rayform.Trigger trigger = target.data.classes.OfType<Rayform.Trigger>().FirstOrDefault();
            if (trigger != null)
            {
                Handles.color = new Color(1, 0.5f, 0);
                Handles.DrawWireCube(target.transform.position, new Vector3((float)trigger.sizeX / 100, (float)trigger.sizeY / 100, (float)trigger.sizeZ / 100));
                HandleUtility.Repaint();
            }
        }
    }

    //private void OnDrawGizmosSelected()
    //{
    //    Rayform.Trigger trigger = data.classes.OfType<Rayform.Trigger>().FirstOrDefault();
    //    if(trigger != null)
    //    {
    //        Gizmos.color = new Color(1, 0.5f, 0);
    //        Gizmos.DrawWireCube(transform.position, new Vector3(trigger.sizeX/100, trigger.sizeY / 100, trigger.sizeZ / 100));
    //
    //    }
    //}
    //private void OnSceneGUI()
    //{
    //    Handles.color = new Color(1, 0.5f, 0);
    //    Handles.DrawWireCube(transform.position, new Vector3(1, 1, 1));
    //}

    public static void ObjectFields(Rayform.ObjClass objClass)
    {
        GUIStyle style = new GUIStyle { richText = true };
        Type[] classTypes = new Type[] { typeof(Rayform.Object), typeof(Rayform.Weapon), typeof(Rayform.Shield), typeof(Rayform.Apparel), typeof(Rayform.Container), typeof(Rayform.Door), typeof(Rayform.Operative), typeof(Rayform.DropTable), typeof(Rayform.Trigger), typeof(Rayform.Map) };
        string[] classNames = classTypes.Select(t => t.Name).ToArray();

        // Classes present to int[]
        int[] arrayMask = objClass.classes.Select(obj => Array.IndexOf(classTypes, obj.GetType())).ToArray();

        // int[] to bitmask
        int mask = 0;
        foreach (int value in arrayMask)
        {
            mask |= (1 << value);
        }
        //Debug.Log(string.Join(", ", arrayMask));

        int newMask = EditorGUI.MaskField(EditorGUILayout.GetControlRect(), "Classes", mask, classNames);

        // Selection has changed, so add or remove the classes
        if (mask != newMask)
        {
            //Debug.Log(mask + " " + newMask);
            List<Type> selectedClasses = new List<Type>();
            List<Type> existingClasses = objClass.classes.Select(obj => obj.GetType()).ToList();
            for (int i = 0; i < classTypes.Length; i++)
            {
                if ((newMask & (1 << i)) != 0)
                {
                    selectedClasses.Add(classTypes[i]);
                }
            }
            foreach (Type classType in classTypes)
            {
                // CLass not present but selected, so add class
                if (!existingClasses.Contains(classType) && selectedClasses.Contains(classType))
                {
                    objClass.classes.Add((Rayform.HeavyChunk)Activator.CreateInstance(classType));
                }
                // CLass present but not selected, so remove class
                if (existingClasses.Contains(classType) && !selectedClasses.Contains(classType))
                {
                    objClass.classes.RemoveAll(obj => obj.GetType() == classType);
                }
                // Order by their type
                objClass.classes = objClass.classes.OrderBy(obj => Array.IndexOf(classTypes, obj.GetType())).ToList();
            }
            //Debug.Log(string.Join(", ", selectedClasses));
        }
        //EditorGUILayout.MaskField(1, options);

        foreach (Rayform.HeavyChunk chunk in objClass.classes)
        {
            GUILayout.BeginVertical(EditorStyles.helpBox);
            EditorGUILayout.LabelField("<b>" + chunk.GetType().Name + "</b>", style);

            if (chunk.GetType() == typeof(Rayform.Thing))
            {
                Rayform.Thing thing = (Rayform.Thing)chunk;
                thing.thingName = EditorGUILayout.TextField("Name", thing.thingName);

                GUILayout.BeginVertical(EditorStyles.helpBox);
                GUILayout.Label("<b>Parent ID</b>", style);
                thing.parentId.baseId = (ushort)EditorGUILayout.IntField("Base Id", thing.parentId.baseId);
                thing.parentId.flags = (Rayform.Id.Flags)EditorGUILayout.EnumPopup("Rdb Flag", thing.parentId.flags);
                GUILayout.EndVertical();

            }
            else if (chunk.GetType() == typeof(Rayform.Object))
            {
                Rayform.Object obj = (Rayform.Object)chunk;
                obj.modelName = EditorGUILayout.TextField("Model Name", obj.modelName);

                GUILayout.BeginVertical(EditorStyles.helpBox);
                GUILayout.Label("<b>Name ID</b>", style);
                obj.nameId.baseId = (ushort)EditorGUILayout.IntField("Base Id", obj.nameId.baseId);
                obj.nameId.flags = (Rayform.Id.Flags)EditorGUILayout.EnumPopup("Rdb Flag", obj.nameId.flags);
                GUILayout.EndVertical();

                GUILayout.BeginVertical(EditorStyles.helpBox);
                GUILayout.Label("<b>Desc ID</b>", style);
                obj.descId.baseId = (ushort)EditorGUILayout.IntField("Base Id", obj.descId.baseId);
                obj.descId.flags = (Rayform.Id.Flags)EditorGUILayout.EnumPopup("Rdb Flag", obj.descId.flags);
                GUILayout.EndVertical();

                obj.size = (byte)EditorGUILayout.IntSlider("Size", obj.size, 0, byte.MaxValue);
                obj.rarity = (byte)EditorGUILayout.IntSlider("Rarity", obj.rarity, 0, byte.MaxValue);
                obj.value = (ushort)EditorGUILayout.IntField("Value", obj.value);
            }

            else if (chunk.GetType() == typeof(Rayform.Apparel))
            {
                Rayform.Apparel apparel = (Rayform.Apparel)chunk;

                apparel.ModelName = EditorGUILayout.TextField("Equipped Model", apparel.ModelName);

                apparel.impactDefence = (byte)EditorGUILayout.IntSlider("Impact Defence", apparel.impactDefence, 0, byte.MaxValue);
                apparel.slashDefence = (byte)EditorGUILayout.IntSlider("Slash Defence", apparel.slashDefence, 0, byte.MaxValue);
                apparel.crushDefence = (byte)EditorGUILayout.IntSlider("Crush Defence", apparel.crushDefence, 0, byte.MaxValue);
                apparel.pierceDefence = (byte)EditorGUILayout.IntSlider("Pierce Defence", apparel.pierceDefence, 0, byte.MaxValue);
                apparel.coverage = (byte)EditorGUILayout.IntSlider("Coverage", apparel.coverage, 0, byte.MaxValue);
                apparel.encumbrance = (byte)EditorGUILayout.IntSlider("Encumbrance", apparel.encumbrance, 0, byte.MaxValue);
                apparel.sound = (Rayform.Apparel.Sound)EditorGUILayout.EnumPopup("Sound", apparel.sound);
                apparel.bulk = (byte)EditorGUILayout.IntSlider("Bulk", apparel.bulk, 0, byte.MaxValue);
                apparel.type = (Rayform.Apparel.ApparelType)EditorGUILayout.EnumPopup("Apparel Type", apparel.type);
                apparel.grade = (byte)EditorGUILayout.IntSlider("Grade", apparel.grade, 0, byte.MaxValue);
                apparel.traits = (Rayform.Apparel.Traits)EditorGUILayout.EnumPopup("Traits", apparel.traits);

                bool rankChanged = false;
                byte rank = (byte)EditorGUILayout.Popup("Rank", apparel.rank / 32, new string[] { "Inept", "Aspirant", "Novice", "Adept", "Expert", "Master" });
                rankChanged = rankChanged | GUI.changed;
                byte points = (byte)EditorGUILayout.IntSlider("Points", apparel.rank % 32, 0, 31);
                rankChanged = rankChanged | GUI.changed;

                if (rankChanged)
                {
                    apparel.rank = (byte)(rank * 32 + points);
                }

                apparel.rngSeed = EditorGUILayout.IntField("Seed", apparel.rngSeed);
                apparel.quality = (byte)EditorGUILayout.IntSlider("Quality", apparel.quality, 0, byte.MaxValue);
                apparel.wear = (byte)EditorGUILayout.IntSlider("Wear", apparel.wear, 0, byte.MaxValue);
                apparel.dirt = (byte)EditorGUILayout.IntSlider("Dirt", apparel.dirt, 0, byte.MaxValue);
                apparel.variance = (byte)EditorGUILayout.IntSlider("Variance", apparel.variance, 0, byte.MaxValue);

                GUILayout.Label("<b>Material Properties</b>", style);
                foreach (Rayform.Apparel.MaterialProperties material in apparel.materials)
                {
                    GUILayout.BeginVertical(EditorStyles.helpBox);
                    material.layer = (byte)EditorGUILayout.IntField("Layer", material.layer);
                    material.part = (byte)EditorGUILayout.IntField("Part", material.part);
                    material.element = (Rayform.Apparel.MaterialProperties.Element)EditorGUILayout.EnumPopup("Element", material.element);
                    material.design = (Rayform.Apparel.MaterialProperties.Design)EditorGUILayout.EnumPopup("Design", material.design);
                    material.material = (Rayform.Apparel.MaterialProperties.Material)EditorGUILayout.EnumPopup("Material", material.material);
                    material.trait = (Rayform.Apparel.MaterialProperties.Trait)EditorGUILayout.EnumPopup("Trait", material.trait);

                    material.color = (byte)EditorGUILayout.IntField("Color", material.color);

                    material.color = Toolbar.ColorPalette(material.color);

                    //Color bgColor = GUI.backgroundColor;
                    //GUILayout.BeginHorizontal();
                    //
                    //GUIStyle btnStyle = new GUIStyle(GUI.skin.box);
                    //btnStyle.fixedWidth = 10;
                    //btnStyle.fixedHeight = 10;
                    //btnStyle.margin = new RectOffset(1, 1, 1, 1);
                    //
                    //for (int i = 0; i < Utils.palette.Count; i++)
                    //{
                    //    if ((i % 8) == 0)
                    //    {
                    //        GUILayout.BeginVertical();
                    //    }
                    //
                    //    GUI.backgroundColor = Utils.palette[i];
                    //    //Debug.Log(Utils.palette[i]);
                    //    if (GUILayout.Button("", btnStyle))
                    //    {
                    //        material.color = (byte)i;
                    //    }
                    //
                    //    if (((i + 1) % 8) == 0 || i == Utils.palette.Count - 1)
                    //    {
                    //        GUILayout.EndVertical();
                    //    }
                    //}
                    //GUILayout.FlexibleSpace();
                    //GUILayout.EndHorizontal();
                    //GUI.backgroundColor = bgColor;

                    //EditorGUI.DrawRect(new Rect(10, 10, 1, 16), Color.red);

                    material.theme = (Rayform.Apparel.MaterialProperties.Theme)EditorGUILayout.EnumPopup("Theme", material.theme);
                    GUILayout.EndVertical();
                }

                GUILayout.BeginVertical(EditorStyles.helpBox);
                GUILayout.Label("<b>Abilities</b>", style);
                apparel.ability = (Rayform.Apparel.Ability)EditorGUILayout.EnumPopup("Ability", apparel.ability);

                foreach (Rayform.Socket socket in apparel.sockets)
                {
                    GUILayout.BeginVertical(EditorStyles.helpBox);
                    socket.type = (Rayform.Socket.Type)EditorGUILayout.EnumPopup("Socket Type", socket.type);
                    socket.crystalUsage = (byte)EditorGUILayout.IntSlider("Crystal Usage", socket.crystalUsage, 0, byte.MaxValue);
                    GUILayout.EndVertical();
                }
                GUILayout.EndVertical();

                GUILayout.BeginVertical(EditorStyles.helpBox);
                GUILayout.Label("<b>Set ID</b>", style);
                apparel.setId.baseId = (ushort)EditorGUILayout.IntField("Base Id", apparel.setId.baseId);
                apparel.setId.flags = (Rayform.Id.Flags)EditorGUILayout.EnumPopup("Rdb Flag", apparel.setId.flags);
                GUILayout.EndVertical();
            }
            else if (chunk.GetType() == typeof(Rayform.Door))
            {
                Rayform.Door door = (Rayform.Door)chunk;
                door.doorFlags = (Rayform.Door.DoorFlag)EditorGUILayout.EnumFlagsField("Door Flags", door.doorFlags);

                GUILayout.BeginVertical(EditorStyles.helpBox);
                GUILayout.Label("<b>Key ID</b>", style);
                door.keyId.baseId = (ushort)EditorGUILayout.IntField("Base Id", door.keyId.baseId);
                door.keyId.flags = (Rayform.Id.Flags)EditorGUILayout.EnumPopup("Rdb Flag", door.keyId.flags);
                GUILayout.EndVertical();
            }
            else if (chunk.GetType() == typeof(Rayform.Trigger))
            {
                Rayform.Trigger trigger = (Rayform.Trigger)chunk;

                GUILayout.BeginVertical(EditorStyles.helpBox);
                GUILayout.Label("<b>Box Size</b>", style);
                trigger.sizeX = (ushort)EditorGUILayout.IntField("X", trigger.sizeX);
                trigger.sizeY = (ushort)EditorGUILayout.IntField("Y", trigger.sizeY);
                trigger.sizeZ = (ushort)EditorGUILayout.IntField("Z", trigger.sizeZ);
                GUILayout.EndVertical();

                trigger.function = (Rayform.Trigger.Function)EditorGUILayout.EnumPopup("Function", trigger.function);
                trigger.trigger = (Rayform.Trigger.TriggerType)EditorGUILayout.EnumPopup("Trigger", trigger.trigger);
                GUILayout.BeginVertical(EditorStyles.helpBox);
                GUILayout.Label("<b>Func ID</b>", style);
                trigger.funcId.baseId = (ushort)EditorGUILayout.IntField("Base Id", trigger.funcId.baseId);
                trigger.funcId.flags = (Rayform.Id.Flags)EditorGUILayout.EnumPopup("Rdb Flag", trigger.funcId.flags);
                GUILayout.EndVertical();

                GUILayout.BeginVertical(EditorStyles.helpBox);
                GUILayout.Label("<b>Code</b>", style);
                trigger.code = EditorGUILayout.TextArea(trigger.code);
                GUILayout.EndVertical();
            }
            else if (chunk.GetType() == typeof(Rayform.Weapon))
            {
                Rayform.Weapon weapon = (Rayform.Weapon)chunk;

                GUILayout.BeginVertical(EditorStyles.helpBox);
                GUILayout.Label("<b>Grips</b>", style);
                weapon.weaponGrips[0] = (Rayform.Weapon.GripType)EditorGUILayout.EnumPopup("Grip Type", weapon.weaponGrips[0]);
                weapon.weaponGrips[1] = (Rayform.Weapon.GripType)EditorGUILayout.EnumPopup("Alt Grip Type", weapon.weaponGrips[1]);
                weapon.gripFlags = (Rayform.Weapon.GripFlags)EditorGUILayout.EnumPopup("Grip Flag", weapon.gripFlags);
                GUILayout.EndVertical();

                weapon.flags = (Rayform.Weapon.Flags)EditorGUILayout.EnumPopup("Flags", weapon.flags);
                weapon.factorySeed = EditorGUILayout.IntField("Factory Seed", weapon.factorySeed);
                weapon.materialSeed = EditorGUILayout.IntField("Material Seed", weapon.factorySeed);
                weapon.quality = (byte)EditorGUILayout.IntSlider("Quality", weapon.quality, 0, byte.MaxValue);
                weapon.wear = (byte)EditorGUILayout.IntSlider("Wear", weapon.wear, 0, byte.MaxValue);
                weapon.dirtSmoothing = (byte)EditorGUILayout.IntSlider("Dirt Smoothing", weapon.dirtSmoothing, 0, byte.MaxValue);
				weapon.weight = (byte)EditorGUILayout.IntSlider("Weight", weapon.weight, 0, byte.MaxValue);
                weapon.balance = (byte)EditorGUILayout.IntSlider("Balance", weapon.balance, 0, byte.MaxValue);
				
				GUILayout.BeginVertical(EditorStyles.helpBox);
				EditorGUILayout.LabelField("<b>Component Selection</b>", style);
				EditorGUILayout.LabelField("  1        2         3         4        VAR     EX1    EX2    EX3", new GUIStyle { fontSize = 8 }, GUILayout.Height(10));
				EditorGUILayout.BeginHorizontal();
				weapon.component1 = (byte)EditorGUILayout.IntField(weapon.component1, GUILayout.Width(20));
				weapon.component2 = (byte)EditorGUILayout.IntField(weapon.component2, GUILayout.Width(20));
				weapon.component3 = (byte)EditorGUILayout.IntField(weapon.component3, GUILayout.Width(20));
				weapon.component4 = (byte)EditorGUILayout.IntField(weapon.component4, GUILayout.Width(20));
				weapon.compVariant = (byte)EditorGUILayout.IntField(weapon.compVariant, GUILayout.Width(20));
				weapon.compExtra1 = (byte)EditorGUILayout.IntField(weapon.compExtra1, GUILayout.Width(20));
				weapon.compExtra2 = (byte)EditorGUILayout.IntField(weapon.compExtra2, GUILayout.Width(20));
				weapon.compUnknown1 = (byte)EditorGUILayout.IntField(weapon.compUnknown1, GUILayout.Width(20));
                EditorGUILayout.EndHorizontal();	
                GUILayout.EndVertical();		

				GUILayout.BeginVertical(EditorStyles.helpBox);
				EditorGUILayout.LabelField("<b>Component Configuration</b>", style);
					GUILayout.BeginVertical(EditorStyles.helpBox);
                    weapon.comp_1_Material = (byte)(Rayform.Weapon.Material)EditorGUILayout.EnumPopup("Material", (Rayform.Weapon.Material)weapon.comp_1_Material);
                    weapon.comp_1_Luster = (byte)EditorGUILayout.IntSlider("Luster", weapon.comp_1_Luster, 0, byte.MaxValue);
                    weapon.comp_1_Color = (byte)EditorGUILayout.IntField("Color", weapon.comp_1_Color);
                    weapon.comp_1_Color = Toolbar.ColorPalette(weapon.comp_1_Color);
					GUILayout.EndVertical();
					
					GUILayout.BeginVertical(EditorStyles.helpBox);
                    weapon.comp_2_Material = (byte)(Rayform.Weapon.Material)EditorGUILayout.EnumPopup("Material", (Rayform.Weapon.Material)weapon.comp_2_Material);
                    weapon.comp_2_Luster = (byte)EditorGUILayout.IntSlider("Luster", weapon.comp_2_Luster, 0, byte.MaxValue);
                    weapon.comp_2_Color = (byte)EditorGUILayout.IntField("Color", weapon.comp_2_Color);
                    weapon.comp_2_Color = Toolbar.ColorPalette(weapon.comp_2_Color);
					GUILayout.EndVertical();
					
					GUILayout.BeginVertical(EditorStyles.helpBox);
                    weapon.comp_3_Material = (byte)(Rayform.Weapon.Material)EditorGUILayout.EnumPopup("Material", (Rayform.Weapon.Material)weapon.comp_3_Material);
                    weapon.comp_3_Luster = (byte)EditorGUILayout.IntSlider("Luster", weapon.comp_3_Luster, 0, byte.MaxValue);
                    weapon.comp_3_Color = (byte)EditorGUILayout.IntField("Color", weapon.comp_3_Color);
                    weapon.comp_3_Color = Toolbar.ColorPalette(weapon.comp_2_Color);
					GUILayout.EndVertical();
					
					GUILayout.BeginVertical(EditorStyles.helpBox);
                    weapon.comp_4_Material = (byte)(Rayform.Weapon.Material)EditorGUILayout.EnumPopup("Material", (Rayform.Weapon.Material)weapon.comp_4_Material);
                    weapon.comp_4_Luster = (byte)EditorGUILayout.IntSlider("Luster", weapon.comp_4_Luster, 0, byte.MaxValue);
                    weapon.comp_4_Color = (byte)EditorGUILayout.IntField("Color", weapon.comp_4_Color);
                    weapon.comp_4_Color = Toolbar.ColorPalette(weapon.comp_4_Color);
					GUILayout.EndVertical();
				GUILayout.EndVertical();

                GUILayout.BeginVertical(EditorStyles.helpBox);
                GUILayout.Label("<b>Damage</b>", style);
                weapon.impactDamage = (byte)EditorGUILayout.IntSlider("Impact Damage", weapon.impactDamage, 0, byte.MaxValue);
                weapon.thrustDamage = (byte)EditorGUILayout.IntSlider("Thrust Damage", weapon.thrustDamage, 0, byte.MaxValue);
                weapon.slashDamage = (byte)EditorGUILayout.IntSlider("Slash Damage", weapon.slashDamage, 0, byte.MaxValue);
                weapon.crushDamage = (byte)EditorGUILayout.IntSlider("Crush Damage", weapon.crushDamage, 0, byte.MaxValue);
                weapon.pierceDamage = (byte)EditorGUILayout.IntSlider("Pierce Damage", weapon.pierceDamage, 0, byte.MaxValue);
                GUILayout.EndVertical();

                GUILayout.BeginVertical(EditorStyles.helpBox);
                GUILayout.Label("<b>Flip Damage</b>", style);
                weapon.flip = EditorGUILayout.Toggle("Flippable", weapon.flip);
                weapon.flipImpactDamage = (byte)EditorGUILayout.IntSlider("Flip Impact Damage", weapon.flipImpactDamage, 0, byte.MaxValue);
                weapon.flipThrustDamage = (byte)EditorGUILayout.IntSlider("Flip Thrust Damage", weapon.flipThrustDamage, 0, byte.MaxValue);
                weapon.flipSlashDamage = (byte)EditorGUILayout.IntSlider("Flip Slash Damage", weapon.flipSlashDamage, 0, byte.MaxValue);
                weapon.flipCrushDamage = (byte)EditorGUILayout.IntSlider("Flip Crush Damage", weapon.flipCrushDamage, 0, byte.MaxValue);
                weapon.flipPierceDamage = (byte)EditorGUILayout.IntSlider("Flip Pierce Damage", weapon.flipPierceDamage, 0, byte.MaxValue);
                GUILayout.EndVertical();

                weapon.sound = (Rayform.Weapon.Sound)EditorGUILayout.EnumPopup("Sound", weapon.sound);

                bool rankChanged = false;
                byte rank = (byte)EditorGUILayout.Popup("Rank", weapon.rank / 32, new string[] { "Inept", "Aspirant", "Novice", "Adept", "Expert", "Master" });
                rankChanged = rankChanged | GUI.changed;
                byte points = (byte)EditorGUILayout.IntSlider("Points", weapon.rank % 32, 0, 31);
                rankChanged = rankChanged | GUI.changed;

                if (rankChanged)
                {
                    weapon.rank = (byte)(rank * 32 + points);
                }

                GUILayout.BeginVertical(EditorStyles.helpBox);
                GUILayout.Label("<b>Abilities</b>", style);
                weapon.ability = (Rayform.Weapon.Ability)EditorGUILayout.EnumPopup("Ability", weapon.ability);

                foreach (Rayform.Socket socket in weapon.sockets)
                {
                    GUILayout.BeginVertical(EditorStyles.helpBox);
                    socket.type = (Rayform.Socket.Type)EditorGUILayout.EnumPopup("Socket Type", socket.type);
                    socket.crystalUsage = (byte)EditorGUILayout.IntSlider("Crystal Usage", socket.crystalUsage, 0, byte.MaxValue);
                    GUILayout.EndVertical();
                }
                GUILayout.EndVertical();

                //weapon.rank = (byte)EditorGUILayout.IntField("Rank", weapon.rank);
            }
            else if (chunk.GetType() == typeof(Rayform.Container))
            {
                Rayform.Container container = (Rayform.Container)chunk;

                container.width = (ushort)EditorGUILayout.IntField("Width", container.width);
                container.height = (ushort)EditorGUILayout.IntField("Height", container.height);

                GUILayout.BeginVertical(EditorStyles.helpBox);
                GUILayout.Label("<b>Key ID</b>", style);
                container.keyId.baseId = (ushort)EditorGUILayout.IntField("Base Id", container.keyId.baseId);
                container.keyId.flags = (Rayform.Id.Flags)EditorGUILayout.EnumPopup("Rdb Flag", container.keyId.flags);
                GUILayout.EndVertical();

                container.capacity = (ushort)EditorGUILayout.IntField("Capacity", container.capacity);
                container.maxSize = (ushort)EditorGUILayout.IntField("Max Size", container.maxSize);

                GUILayout.Label("<b>Items</b>", style);
                Func<Rayform.InventoryItem, Rayform.InventoryItem> func = item =>
                {
                    item.x = (ushort)EditorGUILayout.IntField("X", item.x);
                    item.y = (ushort)EditorGUILayout.IntField("Y", item.y);
                    item.item.flags = (Rayform.ObjClass.Flags)EditorGUILayout.EnumFlagsField("Item Flags", item.item.flags);
                    item.item.id.baseId = (ushort)EditorGUILayout.IntField("Base Id", item.item.id.baseId);
                    item.item.id.flags = (Rayform.Id.Flags)EditorGUILayout.EnumPopup("Rdb Flag", item.item.id.flags);
                    return item;
                };
                Toolbar.ListGUI(container.items, func);

            }
            else if (chunk.GetType() == typeof(Rayform.Operative))
            {
                Rayform.Operative operative = (Rayform.Operative)chunk;

                operative.triggerType = (Rayform.Operative.TriggerType)EditorGUILayout.EnumPopup("Trigger Type", operative.triggerType);
                if (operative.triggerType == Rayform.Operative.TriggerType.Mechanism)
                {
                    operative.mechanismType = (Rayform.Operative.MechanismType)EditorGUILayout.EnumPopup("Mechanism Type", operative.mechanismType);
                    operative.mechanismFlag = (Rayform.Operative.MechanismFlag)EditorGUILayout.EnumPopup("Mechanism Flag", operative.mechanismFlag);
                    operative.mechanismUnk1 = (byte)EditorGUILayout.IntField("Unknown 1", operative.mechanismUnk1);
                    operative.mechanismUnk2 = (byte)EditorGUILayout.IntField("Unknown 2", operative.mechanismUnk2);
                    operative.mechanismUnk3 = (byte)EditorGUILayout.IntField("Unknown 3", operative.mechanismUnk3);
                    operative.mechanismUnk4 = (byte)EditorGUILayout.IntField("Unknown 4", operative.mechanismUnk4);
                    operative.mechanismUnk5 = (byte)EditorGUILayout.IntField("Unknown 5", operative.mechanismUnk5);
                    GUILayout.BeginVertical(EditorStyles.helpBox);
                    operative.targetId1.baseId = (ushort)EditorGUILayout.IntField("Base Id", operative.targetId1.baseId);
                    operative.targetId1.flags = (Rayform.Id.Flags)EditorGUILayout.EnumPopup("Rdb Flag", operative.targetId1.flags);
                    GUILayout.EndVertical();
                    if (operative.mechanismType == Rayform.Operative.MechanismType.TwoTargets || (operative.mechanismType == Rayform.Operative.MechanismType.SuccessOrFailCondition))
                    {
                        GUILayout.BeginVertical(EditorStyles.helpBox);
                        operative.targetId2.baseId = (ushort)EditorGUILayout.IntField("Base Id", operative.targetId2.baseId);
                        operative.targetId2.flags = (Rayform.Id.Flags)EditorGUILayout.EnumPopup("Rdb Flag", operative.targetId2.flags);
                        GUILayout.EndVertical();
                    }
                    else
                    {
                        operative.mechanismUnk6 = EditorGUILayout.IntField("Unknown 6", operative.mechanismUnk6);
                    }
                    operative.sound = EditorGUILayout.TextField("Sound", operative.sound);
                }
                else if (operative.triggerType == Rayform.Operative.TriggerType.Button)
                {
                    operative.operativeType = (Rayform.Operative.OperativeType)EditorGUILayout.EnumPopup("Button Type", (Rayform.Operative.OprtvCategoryPressurePlate)operative.operativeType);
                    operative.pressurePlateFlag = (Rayform.Operative.PressurePlateFlag)EditorGUILayout.EnumPopup("Flag", operative.pressurePlateFlag);
                    operative.plateUnk1 = (byte)EditorGUILayout.IntField("Unknown 1", operative.plateUnk1);
                    operative.plateUnk2 = (ushort)EditorGUILayout.IntField("Unknown 2", operative.plateUnk2);
                    operative.plateUnk4 = (byte)EditorGUILayout.IntField("Unknown 4", operative.plateUnk4);
                    operative.plateUnk5 = (byte)EditorGUILayout.IntField("Unknown 5", operative.plateUnk5);
                    GUILayout.BeginVertical(EditorStyles.helpBox);
                    GUILayout.Label("<b>Target ID 1</b>", style);
                    operative.targetId1.baseId = (ushort)EditorGUILayout.IntField("Base Id", operative.targetId1.baseId);
                    operative.targetId1.flags = (Rayform.Id.Flags)EditorGUILayout.EnumPopup("Rdb Flag", operative.targetId1.flags);
                    GUILayout.EndVertical();
                    GUILayout.BeginVertical(EditorStyles.helpBox);
                    GUILayout.Label("<b>Target ID 2</b>", style);
                    operative.targetId2.baseId = (ushort)EditorGUILayout.IntField("Base Id", operative.targetId2.baseId);
                    operative.targetId2.flags = (Rayform.Id.Flags)EditorGUILayout.EnumPopup("Rdb Flag", operative.targetId2.flags);
                    GUILayout.EndVertical();
                    operative.sound = EditorGUILayout.TextField("Sound", operative.sound);
                }
                else if (operative.triggerType == Rayform.Operative.TriggerType.Lever)
                {
                    operative.operativeType = (Rayform.Operative.OperativeType)EditorGUILayout.EnumPopup("Lever Type", (Rayform.Operative.OprtvCategoryLever)operative.operativeType);
                    operative.leverHeldFlag = (Rayform.Operative.LeverHeldFlag)EditorGUILayout.EnumPopup("Lever Flag", operative.leverHeldFlag); //could be a bitmask, see rayformparser
                    operative.leverHeldUnk1 = (byte)EditorGUILayout.IntField("Unknown 1", operative.leverHeldUnk1);
                    operative.leverHeldUnk2 = (byte)EditorGUILayout.IntField("Unknown 2", operative.leverHeldUnk2);
                    operative.leverHeldUnk3 = (byte)EditorGUILayout.IntField("Unknown 3", operative.leverHeldUnk3);
                    operative.leverHeldTime = (byte)EditorGUILayout.IntField("Time (Centiseconds)", operative.leverHeldTime);
                    operative.leverHeldUnk4 = (byte)EditorGUILayout.IntField("Unknown 4", operative.leverHeldUnk4);
                    GUILayout.BeginVertical(EditorStyles.helpBox);
                    GUILayout.Label("<b>Target ID 1</b>", style);
                    operative.targetId1.baseId = (ushort)EditorGUILayout.IntField("Base Id", operative.targetId1.baseId);
                    operative.targetId1.flags = (Rayform.Id.Flags)EditorGUILayout.EnumPopup("Rdb Flag", operative.targetId1.flags);
                    GUILayout.EndVertical();
                    if (operative.leverHeldFlag == Rayform.Operative.LeverHeldFlag.TwoTargets || (operative.leverHeldFlag == Rayform.Operative.LeverHeldFlag.TwoTargetsInverse))
                    {
                        GUILayout.BeginVertical(EditorStyles.helpBox);
                        GUILayout.Label("<b>Target ID 2</b>", style);
                        operative.targetId2.baseId = (ushort)EditorGUILayout.IntField("Base Id", operative.targetId2.baseId);
                        operative.targetId2.flags = (Rayform.Id.Flags)EditorGUILayout.EnumPopup("Rdb Flag", operative.targetId2.flags);
                        GUILayout.EndVertical();
                    }
                    else
                    {
                        operative.leverHeldUnk5 = EditorGUILayout.IntField("Unknown 5", operative.leverHeldUnk5);
                    }
                    operative.sound = EditorGUILayout.TextField("Sound", operative.sound);
                }
                else if (operative.triggerType == Rayform.Operative.TriggerType.Effect)
                {
                    operative.operativeType = (Rayform.Operative.OperativeType)EditorGUILayout.EnumPopup("Effect Type", (Rayform.Operative.OprtvCategoryEffect)operative.operativeType);
                    if (operative.operativeType == Rayform.Operative.OperativeType.Fire)
                    {
                        operative.fireSize = (ushort)EditorGUILayout.IntSlider("Size", operative.fireSize, 0, 400);
                    }
                    else if (operative.operativeType == Rayform.Operative.OperativeType.Lightning)
                    {
                        operative.lightningFlag = (byte)EditorGUILayout.IntField("Flag", operative.lightningFlag);
                        operative.lightningAnimationType = (Rayform.Operative.LightningAnimationType)EditorGUILayout.EnumPopup("Animation Type", operative.lightningAnimationType);
                        operative.lightningOn = EditorGUILayout.Toggle("On", operative.lightningOn);
                        operative.lightningUnk1 = (byte)EditorGUILayout.IntField("Unknown 1", operative.lightningUnk1);
                        operative.lightningUnk2 = (byte)EditorGUILayout.IntField("Unknown 2", operative.lightningUnk2);
                        operative.lightningUnk3 = (byte)EditorGUILayout.IntField("Unknown 3", operative.lightningUnk3);
                        GUILayout.BeginVertical(EditorStyles.helpBox);
                        GUILayout.Label("<b>Controls</b>", style);
                        operative.lightningUnk4 = (byte)EditorGUILayout.IntField("Unknown 4", operative.lightningUnk4);
                        operative.lightningWidth = (byte)EditorGUILayout.IntField("Width", operative.lightningWidth);
                        operative.lightningLength = (ushort)EditorGUILayout.IntField("Length", operative.lightningLength);
                        operative.lightningIntensity = (ushort)EditorGUILayout.IntField("Intensity", operative.lightningIntensity);
                        GUILayout.EndVertical();
                        operative.lightningUnk5 = (ushort)EditorGUILayout.IntField("Unk2", operative.lightningUnk5);
                        operative.sound = EditorGUILayout.TextField("Sound", operative.sound);
                    }
                }
                else if (operative.triggerType == Rayform.Operative.TriggerType.Item)
                {
                    operative.operativeType = (Rayform.Operative.OperativeType)EditorGUILayout.EnumPopup("Item Type", (Rayform.Operative.OprtvCategoryItem)operative.operativeType);
                    if (operative.operativeType == Rayform.Operative.OperativeType.Key || (operative.operativeType == Rayform.Operative.OperativeType.LeverHeld))
                    {
                        // No data
                        GUILayout.BeginVertical(EditorStyles.helpBox);
                        GUILayout.Label("<b>Target ID 1</b>", style);
                        operative.targetId1.baseId = (ushort)EditorGUILayout.IntField("Base Id", operative.targetId1.baseId);
                        operative.targetId1.flags = (Rayform.Id.Flags)EditorGUILayout.EnumPopup("Rdb Flag", operative.targetId1.flags);
						GUILayout.EndVertical();
						GUILayout.BeginVertical(EditorStyles.helpBox);
						GUILayout.Label("<b>Target ID 2</b>", style);
                        operative.targetId2.baseId = (ushort)EditorGUILayout.IntField("Base Id", operative.targetId2.baseId);
                        operative.targetId2.flags = (Rayform.Id.Flags)EditorGUILayout.EnumPopup("Rdb Flag", operative.targetId2.flags);
						GUILayout.EndVertical();
						operative.sound = EditorGUILayout.TextField("Sound", operative.sound);

                    }
                    else if (operative.operativeType == Rayform.Operative.OperativeType.Salve)
                    {
                        operative.salveHealingTime = (byte)EditorGUILayout.IntField("Healing Amount", operative.salveHealingTime);
                    }
                    else if (operative.operativeType == Rayform.Operative.OperativeType.LeverHeld)
                    {
                        // No data
                    }
                    else if (operative.operativeType == Rayform.Operative.OperativeType.Elixir)
                    {
                        operative.elixirUnk2 = (byte)EditorGUILayout.IntField("Unknown 1", operative.elixirUnk2);
                    }
                    else if (operative.operativeType == Rayform.Operative.OperativeType.BrushScrub)
                    {
                        // No data
                    }
                    else if (operative.operativeType == Rayform.Operative.OperativeType.RepairSphere)
                    {
                        // No data
                    }
                    else if (operative.operativeType == Rayform.Operative.OperativeType.Unknown13)
                    {
                        // No data
                    }
                }
                else if (operative.triggerType == Rayform.Operative.TriggerType.Special)
                {
                    operative.operativeType = (Rayform.Operative.OperativeType)EditorGUILayout.EnumPopup("Operative Type", (Rayform.Operative.OprtvCategorySpecial)operative.operativeType);
                    if (operative.operativeType == Rayform.Operative.OperativeType.Compass)
                    {
                        // No data
                    }
                }
                else if (operative.triggerType == Rayform.Operative.TriggerType.Misc)
                {
                    operative.operativeType = (Rayform.Operative.OperativeType)EditorGUILayout.EnumPopup("Operative Type", (Rayform.Operative.OprtvCategoryMisc)operative.operativeType);
                    {
                        if (operative.operativeType == Rayform.Operative.OperativeType.ClickableTransition)
                        {
                            operative.clickTransitionFlag = (byte)EditorGUILayout.IntField("Flag", operative.clickTransitionFlag);
                            operative.clickTransitionUnk1 = (byte)EditorGUILayout.IntField("Unknown1", operative.clickTransitionUnk1);
                            operative.clickTransitionUnk2 = (byte)EditorGUILayout.IntField("Unknown2", operative.clickTransitionUnk2);
                            operative.clickTransitionUnk3 = (byte)EditorGUILayout.IntField("Unknown3", operative.clickTransitionUnk3);
                            operative.clickTransitionUnk4 = (byte)EditorGUILayout.IntField("Unknown4", operative.clickTransitionUnk4);
                            operative.clickTransitionUnk5 = (byte)EditorGUILayout.IntField("Unknown5", operative.clickTransitionUnk5);
                            GUILayout.BeginVertical(EditorStyles.helpBox);
                            GUILayout.Label("<b>Target ID 1</b>", style);
                            operative.targetId1.baseId = (ushort)EditorGUILayout.IntField("Base Id", operative.targetId1.baseId);
                            operative.targetId1.flags = (Rayform.Id.Flags)EditorGUILayout.EnumPopup("Rdb Flag", operative.targetId1.flags);
                            GUILayout.EndVertical();
                            GUILayout.BeginVertical(EditorStyles.helpBox);
                            GUILayout.Label("<b>Target ID 2</b>", style);
                            operative.targetId2.baseId = (ushort)EditorGUILayout.IntField("Base Id", operative.targetId2.baseId);
                            operative.targetId2.flags = (Rayform.Id.Flags)EditorGUILayout.EnumPopup("Rdb Flag", operative.targetId2.flags);
                            GUILayout.EndVertical();
                            operative.sound = EditorGUILayout.TextField("Sound", operative.sound);
                        }
                        else if (operative.operativeType == Rayform.Operative.OperativeType.LeverHeld)
                        {
                            operative.leverHeldFlag = (Rayform.Operative.LeverHeldFlag)EditorGUILayout.EnumPopup("Switch Flag", operative.leverHeldFlag); //could be a bitmask, see rayformparser
                            operative.leverHeldUnk1 = (byte)EditorGUILayout.IntField("Unknown 1", operative.leverHeldUnk1);
                            operative.leverHeldUnk2 = (byte)EditorGUILayout.IntField("Unknown 2", operative.leverHeldUnk2);
                            operative.leverHeldUnk3 = (byte)EditorGUILayout.IntField("Unknown 3", operative.leverHeldUnk3);
                            operative.leverHeldTime = (byte)EditorGUILayout.IntField("Time (Centiseconds)", operative.leverHeldTime);
                            operative.leverHeldUnk4 = (byte)EditorGUILayout.IntField("Unknown 4", operative.leverHeldUnk4);
                            GUILayout.BeginVertical(EditorStyles.helpBox);
                            GUILayout.Label("<b>Target ID 1</b>", style);
                            operative.targetId1.baseId = (ushort)EditorGUILayout.IntField("Base Id", operative.targetId1.baseId);
                            operative.targetId1.flags = (Rayform.Id.Flags)EditorGUILayout.EnumPopup("Rdb Flag", operative.targetId1.flags);
                            GUILayout.EndVertical();
                            GUILayout.BeginVertical(EditorStyles.helpBox);
                            GUILayout.Label("<b>Target ID 2</b>", style);
                            operative.targetId2.baseId = (ushort)EditorGUILayout.IntField("Base Id", operative.targetId2.baseId);
                            operative.targetId2.flags = (Rayform.Id.Flags)EditorGUILayout.EnumPopup("Rdb Flag", operative.targetId2.flags);
                            GUILayout.EndVertical();
                            operative.sound = EditorGUILayout.TextField("Sound", operative.sound);
                        }
                        else if (operative.operativeType == Rayform.Operative.OperativeType.DivinersDeck)
                        {
                            operative.divinersDeckUnk6 = (byte)EditorGUILayout.IntField("Unknown 1", operative.divinersDeckUnk6);
                            operative.divinersDeckUnk7 = (byte)EditorGUILayout.IntField("Unknown 2", operative.divinersDeckUnk7);
                        }
                    }
                }
                else if (operative.triggerType == Rayform.Operative.TriggerType.Brazier)
                {
                    operative.brazierFlag = (byte)EditorGUILayout.IntField("Flag", operative.brazierFlag);
                    operative.brazierUnk3 = (byte)EditorGUILayout.IntField("Unknown 1", operative.brazierUnk3);
                    operative.brazierUnk4 = (byte)EditorGUILayout.IntField("Unknown 2", operative.brazierUnk4);
                    operative.brazierUnk5 = (byte)EditorGUILayout.IntField("Unknown 3", operative.brazierUnk5);
                    operative.brazierUnk6 = (byte)EditorGUILayout.IntField("Unknown 4", operative.brazierUnk6);
                    operative.sound = EditorGUILayout.TextField("Sound", operative.sound);
                }
                else if (operative.triggerType == Rayform.Operative.TriggerType.Event)
                {
                    operative.operativeType = (Rayform.Operative.OperativeType)EditorGUILayout.EnumPopup("Event Type", (Rayform.Operative.OprtvCategoryEvent)operative.operativeType);
                    if (operative.operativeType == Rayform.Operative.OperativeType.Narrator)
                    {
                        GUILayout.BeginVertical(EditorStyles.helpBox);
                        operative.narratorUnk1 = (byte)EditorGUILayout.IntField("NarratorUnk1", operative.narratorUnk1);
                        operative.narratorUnk2 = (byte)EditorGUILayout.IntField("NarratorUnk2", operative.narratorUnk2);
                        operative.narratorUnk3 = (byte)EditorGUILayout.IntField("NarratorUnk3", operative.narratorUnk3);
                        operative.narratorUnk4 = (byte)EditorGUILayout.IntField("NarratorUnk4", operative.narratorUnk4);
                        GUILayout.EndVertical();
                        operative.sound = EditorGUILayout.TextField("Sound", operative.sound);
                    }
                }
            }
            else if (chunk.GetType() == typeof(Rayform.DropTable))
            {
                Rayform.DropTable droptable = (Rayform.DropTable)chunk;
                droptable.SpclItems = (byte)EditorGUILayout.IntField("Special Items Min", droptable.SpclItems);
                droptable.SpclItems2 = (byte)EditorGUILayout.IntField("Special Items Max", droptable.SpclItems2);

                GUILayout.Label("<b>Items</b>", style);
                Func<Rayform.DropTable.Item, Rayform.DropTable.Item> func = item =>
                {
                    GUILayout.BeginVertical(EditorStyles.helpBox);
                    GUILayout.Label("<b>Item ID</b>", style);
                    item.id.baseId = (ushort)EditorGUILayout.IntField("Base Id", item.id.baseId);
                    item.id.flags = (Rayform.Id.Flags)EditorGUILayout.EnumPopup("Rdb Flag", item.id.flags);
                    GUILayout.EndVertical();
                    item.chance = (byte)EditorGUILayout.IntSlider("Chance", item.chance, 0, byte.MaxValue);
                    item.rarity = (byte)EditorGUILayout.IntSlider("Rarity", item.rarity, 0, byte.MaxValue);
                    item.min = (byte)EditorGUILayout.IntSlider("Min", item.min, 0, byte.MaxValue);
                    item.max = (byte)EditorGUILayout.IntSlider("Max", item.max, 0, byte.MaxValue);
                    item.spawnFlags = (Rayform.DropTable.Item.SpawnFlags)EditorGUILayout.EnumPopup("Spawn Flags", item.spawnFlags);
                    item.SpawnCount = (byte)EditorGUILayout.IntSlider("Spawn Count", item.SpawnCount, 0, byte.MaxValue);
                    item.wearVariance = (byte)EditorGUILayout.IntSlider("Wear Variance", item.wearVariance, 0, byte.MaxValue);
                    item.dirtVariance = (byte)EditorGUILayout.IntSlider("Dirt Variance", item.dirtVariance, 0, byte.MaxValue);
                    //                    item.unknown1 = (byte)EditorGUILayout.IntField("Unk1", item.unknown1);
                    //                  item.unknown2 = (byte)EditorGUILayout.IntField("Unk2", item.unknown2);
                    //                  item.unknown3 = (byte)EditorGUILayout.IntField("Unk3", item.unknown3);
                    //                  item.unknown4 = (byte)EditorGUILayout.IntField("Unk4", item.unknown4);
                    return item;
                };
                Toolbar.ListGUI(droptable.items, func);

            }
            else if (chunk.GetType() == typeof(Rayform.Shield))
            {
                Rayform.Shield shield = (Rayform.Shield)chunk;
                shield.flags = (Rayform.Shield.Flags)EditorGUILayout.EnumPopup("Flags", shield.flags);

				//shield.unknown28 = EditorGUILayout.IntField("Sound", shield.unknown28);
            }
            else if (chunk.GetType() == typeof(Rayform.Map))
            {
                Rayform.Map map = (Rayform.Map)chunk;
                map.mapName = EditorGUILayout.TextField("Map Name", map.mapName);

            }
            GUILayout.EndVertical();
        }

    }
}
